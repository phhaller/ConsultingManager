# ConsultingManager




## Description
This project is designed to help a Sales Consultant in the staffing industry plan and organize it's past, current, and future 
activities both on the candidate and the client side. In addition, activities are tracked and displayed visually in 
different graphs.


## Installation
The project is set up to run on Java 8+ in a MacOS or Windows environment. To change the used Java version simply edit 
the corresponding line (sourceCompatibility) in the build.gradle file and rebuild the project. 
To run the project in a Linux environment, simply add the missing dependencies in the build.gradle file and rebuild the 
project.


## Usage
The project can be executed by running the class Launcher located in the launcher package in an IDE, 
or by running the executable jar file located at the /build/libs folder (java -jar PathfindingVisualizer-1.jar).

Once you successfully started the project you can:

- Tasks: Manage custom tasks in a list view.
- Calendar: Display tasks in calendar view by week.
- Activity: Manage custom activities that you want to achieve in a given time frame.
- Companies: Manage your clients and display activity statistics.
- Jobs: Manage jobs and display activity statistics.
- All candidates: Manage candidates and display process statistics.
- Candidates in process: Manage candidates that currently are in process.
- Notes: Create notes for both the candidate and the client side.
- Statistics: Display activity in different graphs for each year.
- Settings: not yet implemented.


## Roadmap
Working as a Recruitment Consultant myself, I originally made this app purely for my own use and to fit my personal workflow best. 
However, as multiple of my team mates have used / are still using this app as well and provided me with valuable feedback and 
suggestions, there definitely are some interesting additions that can be made. A lot of suggestions evolve around different 
integrations, like being able to access the Outlook calender from the apps calender window. Once I have some spare time I 
might add some of those suggestions.