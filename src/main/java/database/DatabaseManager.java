package database;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;

public class DatabaseManager {


  private String os = System.getProperty("os.name");
  private String userName = System.getProperty("user.name");

  private final String DB_NAME = "ConsultIT_DB.db";
  private String CONNECTION_STRING = "";

  private final String COLUMN_ID = "id";

  private final String TABLE_TASKS = "tasks";
  private final String COLUMN_URGENCY = "urgency";
  private final String COLUMN_DUEDATE = "due_date";
  private final String COLUMN_TASK = "task";
  private final String COLUMN_TASKTYPE = "task_type";


  private final String TABLE_ACTIVITIES = "activities";
  private final String COLUMN_ACTIVITY = "activity";
  private final String COLUMN_GOAL = "goal";
  private final String COLUMN_UNIT = "unit";
  private final String COLUMN_CURRENT = "current";
  private final String COLUMN_CURRENTDATE = "current_date";


  private final String TABLE_ACTIVITIES_STATISTICS = "activities_statistics";
  private final String COLUMN_ACTIVITY_STATISTICS_DATE = "date";
  private final String COLUMN_ACTIVITY_STATISTICS_REFID = "ref_id";
  private final String COLUMN_ACTIVITY_STATISTICS_AMOUNT = "amount";


  private final String TABLE_COMPANIES = "companies";
  private final String COLUMN_COMPANYNAME = "company_name";
  private final String COLUMN_COMPANYDATEADDED = "company_date_added";
  private final String COLUMN_COMPANYLASTMODIFIED = "company_last_modified";


  private final String TABLE_JOBS = "jobs";
  private final String COLUMN_JOBTITLE = "job_title";
  private final String COLUMN_JOBCOMPANY = "job_company";
  private final String COLUMN_JOBURGENCY = "job_urgency";
  private final String COLUMN_JOBDATEADDED = "job_date_added";
  private final String COLUMN_JOBSALARY = "job_salary";
  private final String COLUMN_JOBSTATUS = "job_status";
  private final String COLUMN_JOBLINK = "job_link";
  private final String COLUMN_JOBCVSENT = "job_cv_sent";
  private final String COLUMN_JOBINTERVIEWS = "job_interviews";
  private final String COLUMN_JOBPLACEMENT = "job_placement";


  private final String TABLE_JOBSNOTES = "jobsNotes";
  private final String COLUMN_JOBSNOTESCOMPANY = "company";
  private final String COLUMN_JOBSNOTESJOB = "job";
  private final String COLUMN_JOBSNOTESNOTE = "note";
  private final String COLUMN_JOBSNOTESDATEADDED = "date_added";


  private final String TABLE_CANDIDATESALL = "candidatesAll";
  private final String COLUMN_FIRSTNAME = "first_name";
  private final String COLUMN_LASTNAME = "last_name";
  private final String COLUMN_CANDIDATEJOB = "job";
  private final String COLUMN_CANDIDATESALARY = "salary";
  private final String COLUMN_CANDIDATEPROCESSES = "processes";
  private final String COLUMN_CANDIDATECVSSENT = "cvs_sent";
  private final String COLUMN_CANDIDATEINTERVIEWS = "interviews";
  private final String COLUMN_CANDIDATEPLACEMENTS = "placements";
  private final String COLUMN_CANDIDATEREJECTIONS = "rejections";


  private final String TABLE_CANDIDATESINPROCESS = "candidatesInProcess";
  private final String COLUMN_CANDIDATESINPROCESSNAME = "name";
  private final String COLUMN_CANDIDATESINPROCESSJOB = "job";
  private final String COLUMN_CANDIDATESINPROCESSCOMPANY = "company";
  private final String COLUMN_CANDIDATESINPROCESSSTATUS = "status";
  private final String COLUMN_CANDIDATESINPROCESSLASTMODIFIED = "last_modified";
  private final String COLUMN_CANDIDATESINPROCESSDATEADDED = "date_added";


  private final String TABLE_CANDIDATESNOTES = "candidatesNotes";
  private final String COLUMN_CANDIDATESNOTESNAME = "name";
  private final String COLUMN_CANDIDATESNOTESJOB = "job";
  private final String COLUMN_CANDIDATESNOTESNOTE = "note";
  private final String COLUMN_CANDIDATESNOTESDATEADDED = "date_added";


  private final String TABLE_CANDIDATES_STATISTICS = "candidates_statistics";
  private final String COLUMN_CANDIDATES_STATISTICS_DATE = "date";
  private final String COLUMN_CANDIDATES_STATISTICS_CVSSENT = "cvs_sent";
  private final String COLUMN_CANDIDATES_STATISTICS_INTERVIEWS = "interviews";
  private final String COLUMN_CANDIDATES_STATISTICS_PLACEMENTS = "placements";


  private Statement statement;
  private Connection connection;


  public DatabaseManager() {

    if (os.startsWith("Mac")) {
      // If destination folder does not exist, create it.
      String dbPath = "/Users/" + userName + "/Documents/ConsultIT";
      boolean f = new File(dbPath).mkdirs();
      CONNECTION_STRING = "jdbc:sqlite:" + dbPath + "/" + DB_NAME;
    }

    if (os.startsWith("Windows")) {
      String dbPath = "C:\\Users\\" + userName + "\\Documents\\ConsultIT";
      boolean f = new File(dbPath).mkdirs();
      CONNECTION_STRING = "jdbc:sqlite:" + dbPath + "\\" + DB_NAME;
    }

    try {

      connection = DriverManager.getConnection(CONNECTION_STRING);
      statement = connection.createStatement();

      statement.execute("CREATE TABLE IF NOT EXISTS " +
          TABLE_TASKS + "( " +
          COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
          COLUMN_URGENCY + " TEXT, " +
          COLUMN_DUEDATE + " TEXT, " +
          COLUMN_TASK + " TEXT, " +
          COLUMN_TASKTYPE + " TEXT)");

      statement.execute("CREATE TABLE IF NOT EXISTS " +
          TABLE_ACTIVITIES + "( " +
          COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
          COLUMN_ACTIVITY + " TEXT, " +
          COLUMN_GOAL + " INTEGER, " +
          COLUMN_UNIT + " TEXT, " +
          COLUMN_CURRENT + " INTEGER, " +
          COLUMN_CURRENTDATE + " TEXT)");

      statement.execute("CREATE TABLE IF NOT EXISTS " +
          TABLE_ACTIVITIES_STATISTICS + "( " +
          COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
          COLUMN_ACTIVITY_STATISTICS_DATE + " TEXT, " +
          COLUMN_ACTIVITY_STATISTICS_REFID + " INTEGER, " +
          COLUMN_ACTIVITY_STATISTICS_AMOUNT + " INTEGER)");

      statement.execute("CREATE TABLE IF NOT EXISTS " +
          TABLE_COMPANIES + "( " +
          COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
          COLUMN_COMPANYNAME + " TEXT, " +
          COLUMN_COMPANYDATEADDED + " TEXT, " +
          COLUMN_COMPANYLASTMODIFIED + " TEXT)");

      statement.execute("CREATE TABLE IF NOT EXISTS " +
          TABLE_JOBS + "( " +
          COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
          COLUMN_JOBTITLE + " TEXT, " +
          COLUMN_JOBCOMPANY + " TEXT, " +
          COLUMN_JOBURGENCY + " TEXT, " +
          COLUMN_JOBDATEADDED + " TEXT, " +
          COLUMN_JOBSALARY + " TEXT, " +
          COLUMN_JOBSTATUS + " TEXT, " +
          COLUMN_JOBLINK + " TEXT, " +
          COLUMN_JOBCVSENT + " INTEGER, " +
          COLUMN_JOBINTERVIEWS + " INTEGER, " +
          COLUMN_JOBPLACEMENT + " INTEGER)");

      statement.execute("CREATE TABLE IF NOT EXISTS " +
          TABLE_JOBSNOTES + "( " +
          COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
          COLUMN_JOBSNOTESCOMPANY + " TEXT, " +
          COLUMN_JOBSNOTESJOB + " TEXT, " +
          COLUMN_JOBSNOTESNOTE + " TEXT, " +
          COLUMN_JOBSNOTESDATEADDED + " TEXT)");

      statement.execute("CREATE TABLE IF NOT EXISTS " +
          TABLE_CANDIDATESALL + "( " +
          COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
          COLUMN_FIRSTNAME + " TEXT, " +
          COLUMN_LASTNAME + " TEXT, " +
          COLUMN_CANDIDATEJOB + " TEXT, " +
          COLUMN_CANDIDATESALARY + " TEXT, " +
          COLUMN_CANDIDATEPROCESSES + " INTEGER, " +
          COLUMN_CANDIDATECVSSENT + " INTEGER, " +
          COLUMN_CANDIDATEINTERVIEWS + " INTEGER, " +
          COLUMN_CANDIDATEPLACEMENTS + " INTEGER, " +
          COLUMN_CANDIDATEREJECTIONS + " INTEGER)");

      statement.execute("CREATE TABLE IF NOT EXISTS " +
          TABLE_CANDIDATESINPROCESS + "( " +
          COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
          COLUMN_CANDIDATESINPROCESSNAME + " TEXT, " +
          COLUMN_CANDIDATESINPROCESSJOB + " TEXT, " +
          COLUMN_CANDIDATESINPROCESSCOMPANY + " TEXT, " +
          COLUMN_CANDIDATESINPROCESSSTATUS + " TEXT, " +
          COLUMN_CANDIDATESINPROCESSLASTMODIFIED + " TEXT, " +
          COLUMN_CANDIDATESINPROCESSDATEADDED + " TEXT)");

      statement.execute("CREATE TABLE IF NOT EXISTS " +
          TABLE_CANDIDATESNOTES + "( " +
          COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
          COLUMN_CANDIDATESNOTESNAME + " TEXT, " +
          COLUMN_CANDIDATESNOTESJOB + " TEXT, " +
          COLUMN_CANDIDATESNOTESNOTE + " TEXT, " +
          COLUMN_CANDIDATESNOTESDATEADDED + " TEXT)");

      statement.execute("CREATE TABLE IF NOT EXISTS " +
          TABLE_CANDIDATES_STATISTICS + "( " +
          COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
          COLUMN_CANDIDATES_STATISTICS_DATE + " TEXT, " +
          COLUMN_CANDIDATES_STATISTICS_CVSSENT + " INTEGER, " +
          COLUMN_CANDIDATES_STATISTICS_INTERVIEWS + " INTEGER, " +
          COLUMN_CANDIDATES_STATISTICS_PLACEMENTS + " INTEGER)");

    } catch (SQLException e) {
      e.printStackTrace();
    }

  }




  /* TASKS */
  public void createTaskEntry(String urgency, String due_date, String task, String task_type) throws SQLException {

    statement.execute("INSERT INTO " +
        TABLE_TASKS + " (" + COLUMN_URGENCY + ", " + COLUMN_DUEDATE + ", " + COLUMN_TASK + ", " + COLUMN_TASKTYPE + ") " +
        " VALUES " + "('" +
        urgency + "', '" +
        due_date + "', '" +
        task + "', '" +
        task_type + "')");

  }


  public void updateTaskEntry(int id, String urgency, String due_date, String task, String task_type) throws SQLException {

    statement.execute("UPDATE " + TABLE_TASKS +
        " SET " + COLUMN_URGENCY + " = '" + urgency + "'" + ", " +
        COLUMN_DUEDATE + " = '" + due_date + "'" + ", " +
        COLUMN_TASK + " = '" + task + "'" + ", " +
        COLUMN_TASKTYPE + " = '" + task_type + "'" +
        " WHERE " + COLUMN_ID + " = '" + id + "'"
    );

  }


  public void deleteTaskEntry(int id) throws SQLException {

    statement.execute("DELETE FROM " +
        TABLE_TASKS + " WHERE " +
        COLUMN_ID + " = '" + id + "'" + "COLLATE  NOCASE"
    );

  }


  public ArrayList readTasksEntriesFromSearch(String search) throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_TASKS +
        " WHERE " + COLUMN_DUEDATE + " LIKE '%" + search + "%'" +
        " OR " + COLUMN_TASK + " LIKE '%" + search + "%'" +
        " OR " + COLUMN_TASKTYPE + " LIKE '%" + search + "%'");
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getTaskResults(resultSet);

    return dbEntries;

  }


  public ArrayList readAllTaskEntries() throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_TASKS);
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getTaskResults(resultSet);

    return dbEntries;

  }


  public ArrayList readTaskEntriesByDate(String date) throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_TASKS +
        " WHERE " + COLUMN_DUEDATE + " = '" + date + "'");
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getTaskResults(resultSet);

    return dbEntries;

  }


  private ArrayList<String> getTaskResults(ResultSet resultSet) throws SQLException {

    ArrayList<String> entries = new ArrayList<>();

    while (resultSet.next()) {
      entries.add(resultSet.getInt(COLUMN_ID) + "§" +
          resultSet.getString(COLUMN_URGENCY) + "§" +
          resultSet.getString(COLUMN_DUEDATE) + "§" +
          resultSet.getString(COLUMN_TASK) + "§" +
          resultSet.getString(COLUMN_TASKTYPE)
      );

    }

    return entries;

  }





  /* ACTIVITIES */
  public void createActivitiesEntry(String activity, int goal, String unit, int current, String current_date) throws SQLException {

    statement.execute("INSERT INTO " +
        TABLE_ACTIVITIES + " (" + COLUMN_ACTIVITY + ", " + COLUMN_GOAL + ", " + COLUMN_UNIT + ", " + COLUMN_CURRENT + ", " + COLUMN_CURRENTDATE + ") " +
        " VALUES " + "('" +
        activity + "', '" +
        goal + "', '" +
        unit + "', '" +
        current + "', '" +
        current_date + "')");

  }


  public void updateActivitiesEntryAfterEdit(int id, String activity, int goal, String unit, String current_date) throws SQLException {

    statement.execute("UPDATE " + TABLE_ACTIVITIES +
        " SET " + COLUMN_ACTIVITY + " = '" + activity + "'" + ", " +
        COLUMN_GOAL + " = '" + goal + "'" + ", " +
        COLUMN_UNIT + " = '" + unit + "'" + ", " +
        COLUMN_CURRENTDATE + " = '" + current_date + "'" +
        " WHERE " + COLUMN_ID + " = '" + id + "'"
    );

  }


  public void updateActivitiesEntryCurrent(int id, int current) throws SQLException {

    statement.execute("UPDATE " + TABLE_ACTIVITIES +
        " SET " + COLUMN_CURRENT + " = '" + current + "'" +
        " WHERE " + COLUMN_ID + " = '" + id + "'"
    );

  }


  public void updateActivitiesEntryCurrentDate(int id, String currentDate) throws SQLException {

    statement.execute("UPDATE " + TABLE_ACTIVITIES +
        " SET " + COLUMN_CURRENTDATE + " = '" + currentDate + "'" +
        " WHERE " + COLUMN_ID + " = '" + id + "'"
    );

  }


  public void deleteActivitiesEntry(int id) throws SQLException {

    statement.execute("DELETE FROM " +
        TABLE_ACTIVITIES + " WHERE " +
        COLUMN_ID + " = '" + id + "'" + "COLLATE  NOCASE"
    );

  }


  public String readActivityByID(int id) throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_ACTIVITIES +
        " WHERE " + COLUMN_ID + " = '" + id + "'");
    String dbEntry = "";

    dbEntry = getActivityResults(resultSet);

    return dbEntry;

  }


  public ArrayList readActivitiesEntriesFromSearch(String search) throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_ACTIVITIES +
        " WHERE " + COLUMN_ACTIVITY + " LIKE '%" + search + "%'");
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getActivitiesResults(resultSet);

    return dbEntries;

  }


  public ArrayList readAllActivitiesEntries() throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_ACTIVITIES);
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getActivitiesResults(resultSet);

    return dbEntries;

  }


  private ArrayList<String> getActivitiesResults(ResultSet resultSet) throws SQLException {

    ArrayList<String> entries = new ArrayList<>();

    while (resultSet.next()) {
      entries.add(resultSet.getInt(COLUMN_ID) + "§" +
          resultSet.getString(COLUMN_ACTIVITY) + "§" +
          resultSet.getString(COLUMN_GOAL) + "§" +
          resultSet.getString(COLUMN_UNIT) + "§" +
          resultSet.getString(COLUMN_CURRENT) + "§" +
          resultSet.getString(COLUMN_CURRENTDATE)
      );

    }

    return entries;

  }


  private String getActivityResults(ResultSet resultSet) throws SQLException {

    String result = "";

    while (resultSet.next()) {
      result = resultSet.getInt(COLUMN_ID) + "§" +
          resultSet.getString(COLUMN_ACTIVITY) + "§" +
          resultSet.getString(COLUMN_GOAL) + "§" +
          resultSet.getString(COLUMN_UNIT) + "§" +
          resultSet.getString(COLUMN_CURRENT) + "§" +
          resultSet.getString(COLUMN_CURRENTDATE);

    }

    return result;

  }




  /* ACTIVITIES STATISTICS */
  public void createActivitiesStatisticsEntry(String date, int refId, int amount) throws SQLException {

    statement.execute("INSERT INTO " +
        TABLE_ACTIVITIES_STATISTICS + " (" + COLUMN_ACTIVITY_STATISTICS_DATE + ", " + COLUMN_ACTIVITY_STATISTICS_REFID + ", " + COLUMN_ACTIVITY_STATISTICS_AMOUNT + ") " +
        " VALUES " + "('" +
        date + "', '" +
        refId + "', '" +
        amount + "')");

  }


  public void updateActivitiesStatisticsEntryByRefId(String date, int refId, int amount) throws SQLException {

    statement.execute("UPDATE " + TABLE_ACTIVITIES_STATISTICS +
        " SET " + COLUMN_ACTIVITY_STATISTICS_AMOUNT + " = '" + amount + "'" +
        " WHERE " + COLUMN_ACTIVITY_STATISTICS_DATE + " = '" + date + "'" +
        " AND " + COLUMN_ACTIVITY_STATISTICS_REFID + " = '" + refId + "'"
    );

  }


  public void deleteActivitiesStatisticsEntry(int refId) throws SQLException {

    statement.execute("DELETE FROM " +
        TABLE_ACTIVITIES_STATISTICS + " WHERE " +
        COLUMN_ACTIVITY_STATISTICS_REFID + " = '" + refId + "'"
    );

  }


  public int containsActivitiesStatisticsEntry(String date, int refId) throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT COUNT(*) FROM " + TABLE_ACTIVITIES_STATISTICS +
        " WHERE " + COLUMN_ACTIVITY_STATISTICS_DATE + " = '" + date + "'" +
        " AND " + COLUMN_ACTIVITY_STATISTICS_REFID + " = '" + refId + "'"
        );

    return resultSet.getInt(1);

  }


  public ArrayList readAllActivitiesStatisticsEntriesByRefId(int refId) throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_ACTIVITIES_STATISTICS +
        " WHERE " + COLUMN_ACTIVITY_STATISTICS_REFID + " = '" + refId + "'");
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getActivitiesStatisticsResults(resultSet);

    return dbEntries;

  }


  public String readActivitiesStatisticsEntryByDateAndId(String date, int refId) throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_ACTIVITIES_STATISTICS +
        " WHERE " + COLUMN_ACTIVITY_STATISTICS_DATE + " = '" + date + "'"  +
        " AND " + COLUMN_ACTIVITY_STATISTICS_REFID + " = '" + refId + "'");
    String dbEntry = "";

    dbEntry = getActivityStatisticsResults(resultSet);

    return dbEntry;

  }


  public ArrayList readAllActivitiesStatisticsEntries() throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_ACTIVITIES_STATISTICS);
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getActivitiesStatisticsResults(resultSet);

    return dbEntries;

  }


  private ArrayList<String> getActivitiesStatisticsResults(ResultSet resultSet) throws SQLException {

    ArrayList<String> entries = new ArrayList<>();

    while (resultSet.next()) {
      entries.add(resultSet.getInt(COLUMN_ID) + "§" +
          resultSet.getString(COLUMN_ACTIVITY_STATISTICS_DATE) + "§" +
          resultSet.getString(COLUMN_ACTIVITY_STATISTICS_REFID) + "§" +
          resultSet.getString(COLUMN_ACTIVITY_STATISTICS_AMOUNT)
      );

    }

    return entries;

  }


  private String getActivityStatisticsResults(ResultSet resultSet) throws SQLException {

    String result = "";

    while (resultSet.next()) {
      result = resultSet.getInt(COLUMN_ID) + "§" +
          resultSet.getString(COLUMN_ACTIVITY_STATISTICS_DATE) + "§" +
          resultSet.getString(COLUMN_ACTIVITY_STATISTICS_REFID) + "§" +
          resultSet.getString(COLUMN_ACTIVITY_STATISTICS_AMOUNT);

    }

    return result;

  }




  /* COMPANIES */
  public void createCompaniesEntry(String companyName, String dateAdded, String lastModified) throws SQLException {

    statement.execute("INSERT INTO " +
        TABLE_COMPANIES + " (" + COLUMN_COMPANYNAME + ", " + COLUMN_COMPANYDATEADDED + ", " + COLUMN_COMPANYLASTMODIFIED + ") " +
        " VALUES " + "('" +
        companyName + "', '" +
        dateAdded + "', '" +
        lastModified + "')");

  }


  public void updateCompanyNameEntry(int id, String companyName) throws SQLException {

    statement.execute("UPDATE " + TABLE_COMPANIES +
        " SET " + COLUMN_COMPANYNAME + " = '" + companyName + "'" +
        " WHERE " + COLUMN_ID + " = '" + id + "'"
    );

  }


  public void updateCompanyLastModifiedEntry(String companyName, String lastModified) throws SQLException {

    statement.execute("UPDATE " + TABLE_COMPANIES +
        " SET " + COLUMN_COMPANYLASTMODIFIED + " = '" + lastModified + "'" +
        " WHERE " + COLUMN_COMPANYNAME + " = '" + companyName + "'"
    );

  }


  public void deleteCompaniesEntry(int id) throws SQLException {

    statement.execute("DELETE FROM " +
        TABLE_COMPANIES + " WHERE " +
        COLUMN_ID + " = '" + id + "'"
    );

  }


  public boolean containsCompany(String companyName) throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_COMPANIES +
        " WHERE " + COLUMN_COMPANYNAME + " = '" + companyName + "'" + "COLLATE  NOCASE");

    if (getCompanyResults(resultSet).isEmpty()) {
      return false;
    } else  {
      return true;
    }

  }


  public String readCompanyByID(int id) throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_COMPANIES +
        " WHERE " + COLUMN_ID + " = '" + id + "'");
    String dbEntry = "";

    dbEntry = getCompanyResults(resultSet);

    return dbEntry;

  }


  public ArrayList readCompaniesEntriesFromSearch(String search) throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_COMPANIES +
        " WHERE " + COLUMN_COMPANYNAME + " LIKE '%" + search + "%'");
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getCompaniesResults(resultSet);

    return dbEntries;

  }


  public ArrayList readAllCompaniesEntries() throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_COMPANIES);
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getCompaniesResults(resultSet);

    return dbEntries;

  }


  private ArrayList<String> getCompaniesResults(ResultSet resultSet) throws SQLException {

    ArrayList<String> entries = new ArrayList<>();

    while (resultSet.next()) {
      entries.add(resultSet.getInt(COLUMN_ID) + "§" +
          resultSet.getString(COLUMN_COMPANYNAME) + "§" +
          resultSet.getString(COLUMN_COMPANYDATEADDED) + "§" +
          resultSet.getString(COLUMN_COMPANYLASTMODIFIED)
      );

    }

    return entries;

  }


  private String getCompanyResults(ResultSet resultSet) throws SQLException {

    String result = "";

    while (resultSet.next()) {
      result = resultSet.getInt(COLUMN_ID) + "§" +
          resultSet.getString(COLUMN_COMPANYNAME) + "§" +
          resultSet.getString(COLUMN_COMPANYDATEADDED) + "§" +
          resultSet.getString(COLUMN_COMPANYLASTMODIFIED);

    }

    return result;

  }





  /* JOBS */
  public void createJobsEntry(String job_title, String job_company, String job_urgency, String job_date_added,
                              String job_salary, String job_status, String job_link, int job_cv_sent, int job_interviews,
                              int job_placement) throws SQLException {

    statement.execute("INSERT INTO " +
        TABLE_JOBS + " (" + COLUMN_JOBTITLE + ", " + COLUMN_JOBCOMPANY + ", " + COLUMN_JOBURGENCY + ", " + COLUMN_JOBDATEADDED + ", " + COLUMN_JOBSALARY + ", " + COLUMN_JOBSTATUS + ", " + COLUMN_JOBLINK + ", " + COLUMN_JOBCVSENT + ", " + COLUMN_JOBINTERVIEWS + ", " + COLUMN_JOBPLACEMENT + ") " +
        " VALUES " + "('" +
        job_title + "', '" +
        job_company + "', '" +
        job_urgency + "', '" +
        job_date_added + "', '" +
        job_salary + "', '" +
        job_status + "', '" +
        job_link + "', '" +
        job_cv_sent + "', '" +
        job_interviews + "', '" +
        job_placement + "')");

  }


  public void updateJobsEntryAfterEdit(int id, String job_title, String job_company, String job_urgency, String job_salary,
                                       String job_status, String job_link) throws SQLException {

    statement.execute("UPDATE " + TABLE_JOBS +
        " SET " + COLUMN_JOBTITLE + " = '" + job_title + "'" + ", " +
        COLUMN_JOBCOMPANY + " = '" + job_company + "'" + ", " +
        COLUMN_JOBURGENCY + " = '" + job_urgency + "'" + ", " +
        COLUMN_JOBSALARY + " = '" + job_salary + "'" + ", " +
        COLUMN_JOBSTATUS + " = '" + job_status + "'" + ", " +
        COLUMN_JOBLINK + " = '" + job_link + "'" +
        " WHERE " + COLUMN_ID + " = '" + id + "'"
    );

  }


  public void updateJobsCompanyEntry(String companyName, String newCompanyName) throws SQLException {

    statement.execute("UPDATE " + TABLE_JOBS +
        " SET " + COLUMN_JOBCOMPANY + " = '" + newCompanyName + "'" +
        " WHERE " + COLUMN_JOBCOMPANY + " = '" + companyName + "'"
    );

  }


  public void updateJobsCvSentEntry(int id, int amount) throws SQLException {

    statement.execute("UPDATE " + TABLE_JOBS +
        " SET " + COLUMN_JOBCVSENT + " = '" + amount + "'" +
        " WHERE " + COLUMN_ID + " = '" + id + "'"
    );

  }


  public void updateJobsInterviewsEntry(int id, int amount) throws SQLException {

    statement.execute("UPDATE " + TABLE_JOBS +
        " SET " + COLUMN_JOBINTERVIEWS + " = '" + amount + "'" +
        " WHERE " + COLUMN_ID + " = '" + id + "'"
    );

  }


  public void updateJobsPlacementsEntry(int id, int amount) throws SQLException {

    statement.execute("UPDATE " + TABLE_JOBS +
        " SET " + COLUMN_JOBPLACEMENT + " = '" + amount + "'" +
        " WHERE " + COLUMN_ID + " = '" + id + "'"
    );

  }


  public void deleteJobsEntry(int id) throws SQLException {

    statement.execute("DELETE FROM " +
        TABLE_JOBS + " WHERE " +
        COLUMN_ID + " = '" + id + "'"
    );

  }


  public ArrayList readAllJobsEntries() throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_JOBS);
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getJobsResults(resultSet);

    return dbEntries;

  }


  public ArrayList readJobsEntriesByCompany(String company) throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_JOBS +
        " WHERE " + COLUMN_JOBCOMPANY + " = '" + company + "'");
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getJobsResults(resultSet);

    return dbEntries;

  }


  public ArrayList readJobEntriesFromSearch(String search) throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_JOBS +
        " WHERE " + COLUMN_JOBTITLE + " LIKE '%" + search + "%'" +
        " OR " + COLUMN_JOBCOMPANY + " LIKE '%" + search + "%'");
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getJobsResults(resultSet);

    return dbEntries;

  }


  public String readJobByID(int id) throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_JOBS +
        " WHERE " + COLUMN_ID + " = '" + id + "'");
    String dbEntry = "";

    dbEntry = getJobResults(resultSet);

    return dbEntry;

  }


  private ArrayList<String> getJobsResults(ResultSet resultSet) throws SQLException {

    ArrayList<String> entries = new ArrayList<>();

    while (resultSet.next()) {
      entries.add(resultSet.getInt(COLUMN_ID) + "§" +
          resultSet.getString(COLUMN_JOBTITLE) + "§" +
          resultSet.getString(COLUMN_JOBCOMPANY) + "§" +
          resultSet.getString(COLUMN_JOBURGENCY) + "§" +
          resultSet.getString(COLUMN_JOBDATEADDED) + "§" +
          resultSet.getString(COLUMN_JOBSALARY) + "§" +
          resultSet.getString(COLUMN_JOBSTATUS) + "§" +
          resultSet.getString(COLUMN_JOBLINK) + "§" +
          resultSet.getString(COLUMN_JOBCVSENT) + "§" +
          resultSet.getString(COLUMN_JOBINTERVIEWS) + "§" +
          resultSet.getString(COLUMN_JOBPLACEMENT)
      );

    }

    return entries;

  }


  private String getJobResults(ResultSet resultSet) throws SQLException {

    String result = "";

    while (resultSet.next()) {
      result = resultSet.getInt(COLUMN_ID) + "§" +
          resultSet.getString(COLUMN_JOBTITLE) + "§" +
          resultSet.getString(COLUMN_JOBCOMPANY) + "§" +
          resultSet.getString(COLUMN_JOBURGENCY) + "§" +
          resultSet.getString(COLUMN_JOBDATEADDED) + "§" +
          resultSet.getString(COLUMN_JOBSALARY) + "§" +
          resultSet.getString(COLUMN_JOBSTATUS) + "§" +
          resultSet.getString(COLUMN_JOBLINK) + "§" +
          resultSet.getString(COLUMN_JOBCVSENT) + "§" +
          resultSet.getString(COLUMN_JOBINTERVIEWS) + "§" +
          resultSet.getString(COLUMN_JOBPLACEMENT);

    }

    return result;

  }





  /* JOBS NOTES */
  public void createJobsNotesEntry(String company, String job, String note, String dateAdded) throws SQLException {

    statement.execute("INSERT INTO " +
        TABLE_JOBSNOTES + " (" + COLUMN_JOBSNOTESCOMPANY + ", " + COLUMN_JOBSNOTESJOB + ", " + COLUMN_JOBSNOTESNOTE + ", " + COLUMN_JOBSNOTESDATEADDED + ") " +
        " VALUES " + "('" +
        company + "', '" +
        job + "', '" +
        note + "', '" +
        dateAdded + "')");

  }


  public void updateJobsNotesEntryAfterEdit(int id, String company, String job, String note) throws SQLException {

    statement.execute("UPDATE " + TABLE_JOBSNOTES +
        " SET " + COLUMN_JOBSNOTESCOMPANY + " = '" + company + "'" + ", " +
        COLUMN_JOBSNOTESJOB + " = '" + job + "'" + ", " +
        COLUMN_JOBSNOTESNOTE + " = '" + note + "'" +
        " WHERE " + COLUMN_ID + " = '" + id + "'"
    );

  }


  public void deleteJobsNotesEntry(int id) throws SQLException {

    statement.execute("DELETE FROM " +
        TABLE_JOBSNOTES + " WHERE " +
        COLUMN_ID + " = '" + id + "'"
    );

  }


  public ArrayList readJobsNotesEntriesFromSearch(String search) throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_JOBSNOTES +
        " WHERE " + COLUMN_JOBSNOTESCOMPANY + " LIKE '%" + search + "%'" +
        " OR " + COLUMN_JOBSNOTESJOB + " LIKE '%" + search + "%'" +
        " OR " + COLUMN_JOBSNOTESNOTE + " LIKE '%" + search + "%'");
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getJobsNotesResults(resultSet);

    return dbEntries;

  }


  public ArrayList readAllJobsNotesEntries() throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_JOBSNOTES);
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getJobsNotesResults(resultSet);

    return dbEntries;

  }


  private ArrayList<String> getJobsNotesResults(ResultSet resultSet) throws SQLException {

    ArrayList<String> entries = new ArrayList<>();

    while (resultSet.next()) {
      entries.add(resultSet.getInt(COLUMN_ID) + "§" +
          resultSet.getString(COLUMN_JOBSNOTESCOMPANY) + "§" +
          resultSet.getString(COLUMN_JOBSNOTESJOB) + "§" +
          resultSet.getString(COLUMN_JOBSNOTESNOTE) + "§" +
          resultSet.getString(COLUMN_JOBSNOTESDATEADDED)
      );

    }

    return entries;

  }






  /* CANDIDATES ALL */
  public void createCandidatesAllEntry(String firstName, String lastName, String job, String salary, int processes,
                                       int cvsSent, int interviews, int placements, int rejections) throws SQLException {

    statement.execute("INSERT INTO " +
        TABLE_CANDIDATESALL + " (" + COLUMN_FIRSTNAME + ", " + COLUMN_LASTNAME + ", " + COLUMN_CANDIDATEJOB + ", " + COLUMN_CANDIDATESALARY + ", " + COLUMN_CANDIDATEPROCESSES + ", " + COLUMN_CANDIDATECVSSENT + ", " + COLUMN_CANDIDATEINTERVIEWS + ", " + COLUMN_CANDIDATEPLACEMENTS + ", " + COLUMN_CANDIDATEREJECTIONS + ") " +
        " VALUES " + "('" +
        firstName + "', '" +
        lastName + "', '" +
        job + "', '" +
        salary + "', '" +
        processes + "', '" +
        cvsSent + "', '" +
        interviews + "', '" +
        placements + "', '" +
        rejections + "')");

  }


  public void updateCandidatesAllEntryAfterEdit(int id, String firstName, String lastName, String job, String salary) throws SQLException {

    statement.execute("UPDATE " + TABLE_CANDIDATESALL +
        " SET " + COLUMN_FIRSTNAME + " = '" + firstName + "'" + ", " +
        COLUMN_LASTNAME + " = '" + lastName + "'" + ", " +
        COLUMN_CANDIDATEJOB + " = '" + job + "'" + ", " +
        COLUMN_CANDIDATESALARY + " = '" + salary + "'" +
        " WHERE " + COLUMN_ID + " = '" + id + "'"
    );

  }


  public void updateCandidatesAllProcesses(int id, int processes) throws SQLException {

    statement.execute("UPDATE " + TABLE_CANDIDATESALL +
        " SET " + COLUMN_CANDIDATEPROCESSES + " = '" + processes + "'" +
        " WHERE " + COLUMN_ID + " = '" + id + "'"
    );

  }


  public void updateCandidatesAllCvsSent(int id, int cvsSent) throws SQLException {

    statement.execute("UPDATE " + TABLE_CANDIDATESALL +
        " SET " + COLUMN_CANDIDATECVSSENT + " = '" + cvsSent + "'" +
        " WHERE " + COLUMN_ID + " = '" + id + "'"
    );

  }


  public void updateCandidatesAllInterviews(int id, int interviews) throws SQLException {

    statement.execute("UPDATE " + TABLE_CANDIDATESALL +
        " SET " + COLUMN_CANDIDATEINTERVIEWS + " = '" + interviews + "'" +
        " WHERE " + COLUMN_ID + " = '" + id + "'"
    );

  }


  public void updateCandidatesAllPlacements(int id, int placements) throws SQLException {

    statement.execute("UPDATE " + TABLE_CANDIDATESALL +
        " SET " + COLUMN_CANDIDATEPLACEMENTS + " = '" + placements + "'" +
        " WHERE " + COLUMN_ID + " = '" + id + "'"
    );

  }


  public void updateCandidatesAllRejections(int id, int rejections) throws SQLException {

    statement.execute("UPDATE " + TABLE_CANDIDATESALL +
        " SET " + COLUMN_CANDIDATEREJECTIONS + " = '" + rejections + "'" +
        " WHERE " + COLUMN_ID + " = '" + id + "'"
    );

  }


  public void deleteCandidatesAllEntry(int id) throws SQLException {

    statement.execute("DELETE FROM " +
        TABLE_CANDIDATESALL + " WHERE " +
        COLUMN_ID + " = '" + id + "'"
    );

  }


  public ArrayList readCandidatesAllEntriesFromSearch(String search) throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_CANDIDATESALL +
        " WHERE " + COLUMN_FIRSTNAME + " LIKE '%" + search + "%'" +
        " OR " + COLUMN_LASTNAME + " LIKE '%" + search + "%'");
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getCandidatesAllResults(resultSet);

    return dbEntries;

  }


  public ArrayList readAllCandidatesAllEntries() throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_CANDIDATESALL);
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getCandidatesAllResults(resultSet);

    return dbEntries;

  }


  public String readCandidatesAllByID(int id) throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_CANDIDATESALL +
        " WHERE " + COLUMN_ID + " = '" + id + "'");
    String dbEntry = "";

    dbEntry = getCandidateAllResults(resultSet);

    return dbEntry;

  }


  private ArrayList<String> getCandidatesAllResults(ResultSet resultSet) throws SQLException {

    ArrayList<String> entries = new ArrayList<>();

    while (resultSet.next()) {
      entries.add(resultSet.getInt(COLUMN_ID) + "§" +
          resultSet.getString(COLUMN_FIRSTNAME) + "§" +
          resultSet.getString(COLUMN_LASTNAME) + "§" +
          resultSet.getString(COLUMN_CANDIDATEJOB) + "§" +
          resultSet.getString(COLUMN_CANDIDATESALARY) + "§" +
          resultSet.getString(COLUMN_CANDIDATEPROCESSES) + "§" +
          resultSet.getString(COLUMN_CANDIDATECVSSENT) + "§" +
          resultSet.getString(COLUMN_CANDIDATEINTERVIEWS) + "§" +
          resultSet.getString(COLUMN_CANDIDATEPLACEMENTS) + "§" +
          resultSet.getString(COLUMN_CANDIDATEREJECTIONS)
      );

    }

    return entries;

  }


  private String getCandidateAllResults(ResultSet resultSet) throws SQLException {

    String result = "";

    while (resultSet.next()) {
      result = resultSet.getInt(COLUMN_ID) + "§" +
          resultSet.getString(COLUMN_FIRSTNAME) + "§" +
          resultSet.getString(COLUMN_LASTNAME) + "§" +
          resultSet.getString(COLUMN_CANDIDATEJOB) + "§" +
          resultSet.getString(COLUMN_CANDIDATESALARY) + "§" +
          resultSet.getString(COLUMN_CANDIDATEPROCESSES) + "§" +
          resultSet.getString(COLUMN_CANDIDATECVSSENT) + "§" +
          resultSet.getString(COLUMN_CANDIDATEINTERVIEWS) + "§" +
          resultSet.getString(COLUMN_CANDIDATEPLACEMENTS) + "§" +
          resultSet.getString(COLUMN_CANDIDATEREJECTIONS);

    }

    return result;

  }





  /* CANDIDATES IN PROCESS */
  public void createCandidatesInProcessEntry(String name, String job, String company, String status,
                                       String lastModified, String dateAdded) throws SQLException {

    statement.execute("INSERT INTO " +
        TABLE_CANDIDATESINPROCESS + " (" + COLUMN_CANDIDATESINPROCESSNAME + ", " + COLUMN_CANDIDATESINPROCESSJOB + ", " + COLUMN_CANDIDATESINPROCESSCOMPANY + ", " + COLUMN_CANDIDATESINPROCESSSTATUS + ", " + COLUMN_CANDIDATESINPROCESSLASTMODIFIED + ", " + COLUMN_CANDIDATESINPROCESSDATEADDED + ") " +
        " VALUES " + "('" +
        name + "', '" +
        job + "', '" +
        company + "', '" +
        status + "', '" +
        lastModified + "', '" +
        dateAdded + "')");

  }


  public void updateCandidatesInProcessEntryAfterEdit(int id, String name, String job, String company, String status, String lastModified) throws SQLException {

    statement.execute("UPDATE " + TABLE_CANDIDATESINPROCESS +
        " SET " + COLUMN_CANDIDATESINPROCESSNAME + " = '" + name + "'" + ", " +
        COLUMN_CANDIDATESINPROCESSJOB + " = '" + job + "'" + ", " +
        COLUMN_CANDIDATESINPROCESSCOMPANY + " = '" + company + "'" + ", " +
        COLUMN_CANDIDATESINPROCESSSTATUS + " = '" + status + "'" + ", " +
        COLUMN_CANDIDATESINPROCESSLASTMODIFIED + " = '" + lastModified + "'" +
        " WHERE " + COLUMN_ID + " = '" + id + "'"
    );

  }


  public void deleteCandidatesInProcessEntry(int id) throws SQLException {

    statement.execute("DELETE FROM " +
        TABLE_CANDIDATESINPROCESS + " WHERE " +
        COLUMN_ID + " = '" + id + "'"
    );

  }


  public ArrayList readCandidatesInProcessEntriesFromSearch(String search) throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_CANDIDATESINPROCESS +
        " WHERE " + COLUMN_CANDIDATESINPROCESSNAME + " LIKE '%" + search + "%'" +
        " OR " + COLUMN_CANDIDATESINPROCESSJOB + " LIKE '%" + search + "%'" +
        " OR " + COLUMN_CANDIDATESINPROCESSCOMPANY + " LIKE '%" + search + "%'" +
        " OR " + COLUMN_CANDIDATESINPROCESSSTATUS + " LIKE '%" + search + "%'");
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getCandidatesInProcessResults(resultSet);

    return dbEntries;

  }


  public ArrayList readCandidatesInProcessEntriesByCandidate(String name) throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_CANDIDATESINPROCESS +
        " WHERE " + COLUMN_CANDIDATESINPROCESSNAME + " = '" + name + "'");
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getCandidatesInProcessResults(resultSet);

    return dbEntries;

  }


  public ArrayList readAllCandidatesInProcessEntries() throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_CANDIDATESINPROCESS);
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getCandidatesInProcessResults(resultSet);

    return dbEntries;

  }


  private ArrayList<String> getCandidatesInProcessResults(ResultSet resultSet) throws SQLException {

    ArrayList<String> entries = new ArrayList<>();

    while (resultSet.next()) {
      entries.add(resultSet.getInt(COLUMN_ID) + "§" +
          resultSet.getString(COLUMN_CANDIDATESINPROCESSNAME) + "§" +
          resultSet.getString(COLUMN_CANDIDATESINPROCESSJOB) + "§" +
          resultSet.getString(COLUMN_CANDIDATESINPROCESSCOMPANY) + "§" +
          resultSet.getString(COLUMN_CANDIDATESINPROCESSSTATUS) + "§" +
          resultSet.getString(COLUMN_CANDIDATESINPROCESSLASTMODIFIED) + "§" +
          resultSet.getString(COLUMN_CANDIDATESINPROCESSDATEADDED)
      );

    }

    return entries;

  }





  /* CANDIDATES NOTES */
  public void createCandidatesNotesEntry(String name, String job, String note, String dateAdded) throws SQLException {

    statement.execute("INSERT INTO " +
        TABLE_CANDIDATESNOTES + " (" + COLUMN_CANDIDATESNOTESNAME + ", " + COLUMN_CANDIDATESNOTESJOB + ", " + COLUMN_CANDIDATESNOTESNOTE + ", " + COLUMN_CANDIDATESNOTESDATEADDED + ") " +
        " VALUES " + "('" +
        name + "', '" +
        job + "', '" +
        note + "', '" +
        dateAdded + "')");

  }


  public void updateCandidatesNotesEntryAfterEdit(int id, String company, String job, String note) throws SQLException {

    statement.execute("UPDATE " + TABLE_CANDIDATESNOTES +
        " SET " + COLUMN_CANDIDATESNOTESNAME + " = '" + company + "'" + ", " +
        COLUMN_CANDIDATESNOTESJOB + " = '" + job + "'" + ", " +
        COLUMN_CANDIDATESNOTESNOTE + " = '" + note + "'" +
        " WHERE " + COLUMN_ID + " = '" + id + "'"
    );

  }


  public void deleteCandidatesNotesEntry(int id) throws SQLException {

    statement.execute("DELETE FROM " +
        TABLE_CANDIDATESNOTES + " WHERE " +
        COLUMN_ID + " = '" + id + "'"
    );

  }


  public ArrayList readCandidatesNotesEntriesFromSearch(String search) throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_CANDIDATESNOTES +
        " WHERE " + COLUMN_CANDIDATESNOTESNAME + " LIKE '%" + search + "%'" +
        " OR " + COLUMN_CANDIDATESNOTESJOB + " LIKE '%" + search + "%'" +
        " OR " + COLUMN_CANDIDATESNOTESNOTE + " LIKE '%" + search + "%'");
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getCandidatesNotesResults(resultSet);

    return dbEntries;

  }


  public ArrayList readAllCandidatesNotesEntries() throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_CANDIDATESNOTES);
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getCandidatesNotesResults(resultSet);

    return dbEntries;

  }


  private ArrayList<String> getCandidatesNotesResults(ResultSet resultSet) throws SQLException {

    ArrayList<String> entries = new ArrayList<>();

    while (resultSet.next()) {
      entries.add(resultSet.getInt(COLUMN_ID) + "§" +
          resultSet.getString(COLUMN_CANDIDATESNOTESNAME) + "§" +
          resultSet.getString(COLUMN_CANDIDATESNOTESJOB) + "§" +
          resultSet.getString(COLUMN_CANDIDATESNOTESNOTE) + "§" +
          resultSet.getString(COLUMN_CANDIDATESNOTESDATEADDED)
      );

    }

    return entries;

  }





  /* CANDIDATES STATISTICS */
  public void createCandidatesStatisticsEntry(String date, int cvsSent, int interviews, int placements) throws SQLException {

    statement.execute("INSERT INTO " +
        TABLE_CANDIDATES_STATISTICS + " (" + COLUMN_CANDIDATES_STATISTICS_DATE + ", " + COLUMN_CANDIDATES_STATISTICS_CVSSENT + ", " + COLUMN_CANDIDATES_STATISTICS_INTERVIEWS + ", " + COLUMN_CANDIDATES_STATISTICS_PLACEMENTS + ") " +
        " VALUES " + "('" +
        date + "', '" +
        cvsSent + "', '" +
        interviews + "', '" +
        placements + "')");

  }


  public void updateCandidatesStatisticsEntryByDate(String date, int cvsSent, int interviews, int placements) throws SQLException {

    statement.execute("UPDATE " + TABLE_CANDIDATES_STATISTICS +
        " SET " + COLUMN_CANDIDATES_STATISTICS_CVSSENT + " = '" + cvsSent + "'" + ", " +
        COLUMN_CANDIDATES_STATISTICS_INTERVIEWS + " = '" + interviews + "'" + ", " +
        COLUMN_CANDIDATES_STATISTICS_PLACEMENTS + " = '" + placements + "'" +
        " WHERE " + COLUMN_CANDIDATES_STATISTICS_DATE + " = '" + date + "'"
    );

  }


  public int containsCandidatesStatisticsEntry(String date) throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT COUNT(*) FROM " + TABLE_CANDIDATES_STATISTICS +
        " WHERE " + COLUMN_ACTIVITY_STATISTICS_DATE + " = '" + date + "'"
    );

    return resultSet.getInt(1);

  }


  public String readCandidatesStatisticsEntryByDate(String date) throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_CANDIDATES_STATISTICS +
        " WHERE " + COLUMN_CANDIDATES_STATISTICS_DATE + " = '" + date + "'");
    String dbEntry = "";

    dbEntry = getCandidateStatisticsResults(resultSet);

    return dbEntry;

  }


  public ArrayList readAllCandidatesStatisticsEntries() throws SQLException {

    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_CANDIDATES_STATISTICS);
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getCandidatesStatisticsResults(resultSet);

    return dbEntries;

  }


  private ArrayList<String> getCandidatesStatisticsResults(ResultSet resultSet) throws SQLException {

    ArrayList<String> entries = new ArrayList<>();

    while (resultSet.next()) {
      entries.add(resultSet.getInt(COLUMN_ID) + "§" +
          resultSet.getString(COLUMN_CANDIDATES_STATISTICS_DATE) + "§" +
          resultSet.getString(COLUMN_CANDIDATES_STATISTICS_CVSSENT) + "§" +
          resultSet.getString(COLUMN_CANDIDATES_STATISTICS_INTERVIEWS) + "§" +
          resultSet.getString(COLUMN_CANDIDATES_STATISTICS_PLACEMENTS)
      );

    }

    return entries;

  }


  private String getCandidateStatisticsResults(ResultSet resultSet) throws SQLException {

    String result = "";

    while (resultSet.next()) {
      result = resultSet.getInt(COLUMN_ID) + "§" +
          resultSet.getString(COLUMN_CANDIDATES_STATISTICS_DATE) + "§" +
          resultSet.getString(COLUMN_CANDIDATES_STATISTICS_CVSSENT) + "§" +
          resultSet.getString(COLUMN_CANDIDATES_STATISTICS_INTERVIEWS) + "§" +
          resultSet.getString(COLUMN_CANDIDATES_STATISTICS_PLACEMENTS);

    }

    return result;

  }









  public void close(Connection connection) {
    try {
      if (connection != null) {
        connection.close();
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public void close(Statement statement) {
    try {
      if (statement != null) {
        statement.close();
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public void close(ResultSet resultSet) {
    try {
      if (resultSet != null) {
        resultSet.close();
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }





}
