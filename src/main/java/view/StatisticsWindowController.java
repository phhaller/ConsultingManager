package view;

import database.DatabaseManager;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Line;
import javafx.util.StringConverter;
import javafx.util.converter.IntegerStringConverter;
import util.CustomAreaChart;
import util.DateUtils;

import javax.sound.midi.Soundbank;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class StatisticsWindowController {

  private Scene scene;
  private DatabaseManager databaseManager;
  private HashMap<Integer, Integer> cvsSentMap;
  private HashMap<Integer, Integer> interviewsMap;
  private HashMap<Integer, Integer> placementsMap;
  private HashMap<Integer, Integer> meetingsMap;
  private HashMap<Integer, Integer> companiesMap;
  private HashMap<Integer, Integer> jobsMap;
  private HashMap<String, HashMap<Integer, Integer>> activitiesWeeksMapsList;

  private XYChart.Series cvsSentSeries;
  private XYChart.Series interviewsSeries;
  private XYChart.Series placementsSeries;
  private XYChart.Series meetingsSeries;
  private XYChart.Series companiesSeries;
  private XYChart.Series jobsSeries;
  private HashMap<String, XYChart.Series> activitiesSeriesMap;

  @FXML
  private AnchorPane background, topBar;


  public void initialize(Scene scene, DatabaseManager databaseManager) {

    this.scene = scene;
    this.databaseManager = databaseManager;

    cvsSentMap = new HashMap<>();
    interviewsMap = new HashMap<>();
    placementsMap = new HashMap<>();
    meetingsMap = new HashMap<>();
    companiesMap = new HashMap<>();
    jobsMap = new HashMap<>();
    activitiesWeeksMapsList = new HashMap<>();

    cvsSentSeries = new XYChart.Series();
    interviewsSeries = new XYChart.Series();
    placementsSeries = new XYChart.Series();
    meetingsSeries = new XYChart.Series();
    companiesSeries = new XYChart.Series();
    jobsSeries = new XYChart.Series();
    activitiesSeriesMap = new HashMap<>();

    fetchCandidateData();
    fetchMeetingsData();
    fetchCompaniesData();
    fetchJobsData();
    fetchActivityData();

    setUpCharts();

    createCharts();
    createHandlers();

  }


  private void fetchCandidateData() {

    try {

      ArrayList<String> candidatesStatistics = databaseManager.readAllCandidatesStatisticsEntries();

      for (String candidateStatistics : candidatesStatistics) {

        String[] c = candidateStatistics.split("§");

        if (DateUtils.getYearFromDate(DateUtils.getDateFromString(c[1])).equals(DateUtils.getCurrentYear())) {

          int week = Integer.parseInt(DateUtils.getWeekFromDate(DateUtils.getDateFromString(c[1])));

          if (!cvsSentMap.containsKey(week)) {
            cvsSentMap.put(week, Integer.parseInt(c[2]));
          } else {
            cvsSentMap.put(week, cvsSentMap.get(week) + Integer.parseInt(c[2]));
          }

          if (!interviewsMap.containsKey(week)) {
            interviewsMap.put(week, Integer.parseInt(c[3]));
          } else {
            interviewsMap.put(week, interviewsMap.get(week) + Integer.parseInt(c[3]));
          }

          if (!placementsMap.containsKey(week)) {
            placementsMap.put(week, Integer.parseInt(c[4]));
          } else {
            placementsMap.put(week, placementsMap.get(week) + Integer.parseInt(c[4]));
          }

        }

      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

  }


  private void fetchMeetingsData() {

    try {

      ArrayList<String> meetingsStatistics = databaseManager.readAllTaskEntries();

      for (String meetingStatistics : meetingsStatistics) {

        String[] m = meetingStatistics.split("§");

        if (DateUtils.getYearFromDate(DateUtils.getDateFromString(m[2])).equals(DateUtils.getCurrentYear())) {

          int week = Integer.parseInt(DateUtils.getWeekFromDate(DateUtils.getDateFromString(m[2])));

          if (m[4].equalsIgnoreCase("meeting")) {

            if (!meetingsMap.containsKey(week)) {
              meetingsMap.put(week, 1);
            } else {
              meetingsMap.put(week, meetingsMap.get(week) + 1);
            }

          }

        }

      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

  }


  private void fetchCompaniesData() {

    try {

      ArrayList<String> companiesStatistics = databaseManager.readAllCompaniesEntries();

      for (String companyStatistics : companiesStatistics) {

        String[] c = companyStatistics.split("§");

        if (DateUtils.getYearFromDate(DateUtils.getDateFromString(c[2])).equals(DateUtils.getCurrentYear())) {

          int week = Integer.parseInt(DateUtils.getWeekFromDate(DateUtils.getDateFromString(c[2])));

          if (!companiesMap.containsKey(week)) {
            companiesMap.put(week, 1);
          } else {
            companiesMap.put(week, companiesMap.get(week) + 1);
          }

        }

      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

  }


  private void fetchJobsData() {

    try {

      ArrayList<String> jobsStatistics = databaseManager.readAllJobsEntries();

      for (String jobStatistics : jobsStatistics) {

        String[] j = jobStatistics.split("§");

        if (DateUtils.getYearFromDate(DateUtils.getDateFromString(j[4])).equals(DateUtils.getCurrentYear())) {

          int week = Integer.parseInt(DateUtils.getWeekFromDate(DateUtils.getDateFromString(j[4])));

          if (!jobsMap.containsKey(week)) {
            jobsMap.put(week, 1);
          } else {
            jobsMap.put(week, jobsMap.get(week) + 1);
          }

        }

      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

  }


  private void fetchActivityData() {

    try {

      ArrayList<String> activities = databaseManager.readAllActivitiesEntries();

      for (String activity : activities) {

        String[] a = activity.split("§");
        activitiesSeriesMap.put(a[1], new XYChart.Series());
        ArrayList<String> statistics = databaseManager.readAllActivitiesStatisticsEntriesByRefId(Integer.parseInt(a[0]));

        HashMap<Integer, Integer> activitiesWeekMap = new HashMap<>();

        for (String statistic : statistics) {
          String[] s = statistic.split("§");

          if (DateUtils.getYearFromDate(DateUtils.getDateFromString(s[1])).equals(DateUtils.getCurrentYear())) {

            int week = Integer.parseInt(DateUtils.getWeekFromDate(DateUtils.getDateFromString(s[1])));

            if (!activitiesWeekMap.containsKey(week)) {
              activitiesWeekMap.put(week, Integer.parseInt(s[3]));
            } else {
              activitiesWeekMap.put(week, activitiesWeekMap.get(week) + Integer.parseInt(s[3]));
            }

          }

        }

        activitiesWeeksMapsList.put(a[1], activitiesWeekMap);

      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

  }


  private void setUpCharts() {

    for (int i = 1; i <= 53; i++) {

      if (cvsSentMap.keySet().contains(i)) {
        cvsSentSeries.getData().add(new XYChart.Data(String.valueOf(i), cvsSentMap.get(i)));
      } else {
        cvsSentSeries.getData().add(new XYChart.Data(String.valueOf(i), 0));
      }

      if (interviewsMap.keySet().contains(i)) {
        interviewsSeries.getData().add(new XYChart.Data(String.valueOf(i), interviewsMap.get(i)));
      } else {
        interviewsSeries.getData().add(new XYChart.Data(String.valueOf(i), 0));
      }

      if (placementsMap.keySet().contains(i)) {
        placementsSeries.getData().add(new XYChart.Data(String.valueOf(i), placementsMap.get(i)));
      } else {
        placementsSeries.getData().add(new XYChart.Data(String.valueOf(i), 0));
      }

      if (meetingsMap.keySet().contains(i)) {
        meetingsSeries.getData().add(new XYChart.Data(String.valueOf(i), meetingsMap.get(i)));
      } else {
        meetingsSeries.getData().add(new XYChart.Data(String.valueOf(i), 0));
      }

      if (companiesMap.keySet().contains(i)) {
        companiesSeries.getData().add(new XYChart.Data(String.valueOf(i), companiesMap.get(i)));
      } else {
        companiesSeries.getData().add(new XYChart.Data(String.valueOf(i), 0));
      }

      if (jobsMap.keySet().contains(i)) {
        jobsSeries.getData().add(new XYChart.Data(String.valueOf(i), jobsMap.get(i)));
      } else {
        jobsSeries.getData().add(new XYChart.Data(String.valueOf(i), 0));
      }

      for (String activity : activitiesWeeksMapsList.keySet()) {

        XYChart.Series series = activitiesSeriesMap.get(activity);
        if (activitiesWeeksMapsList.get(activity).keySet().contains(i)) {
          series.getData().add(new XYChart.Data(String.valueOf(i), activitiesWeeksMapsList.get(activity).get(i)));
        } else {
          series.getData().add(new XYChart.Data(String.valueOf(i), 0));
        }

      }

    }

  }


  private void createCharts() {

    createAreaChart(15, 110, 303, 200, cvsSentSeries, "CVs sent");
    createAreaChart(323, 110, 303, 200, interviewsSeries, "Interviews");
    createAreaChart(621, 110, 303, 200, placementsSeries, "Placements");

    createAreaChart(15, 340, 303, 200, meetingsSeries, "Meetings");
    createAreaChart(323, 340, 303, 200, jobsSeries, "Jobs");
    createAreaChart(621, 340, 303, 200, companiesSeries, "Clients");

    createAreaChart(15, 570, 920, 200, activitiesSeriesMap, "Activities");

  }


  private void createAreaChart(int x, int y, int width, int height, XYChart.Series data, String label) {

    CategoryAxis xAxis = new CategoryAxis();
    xAxis.setTickLabelsVisible(false);
    NumberAxis yAxis = new NumberAxis();
    yAxis.setTickLabelFormatter(new StringConverter<Number>() {
      @Override
      public String toString(Number number) {
        if(number.intValue()!=number.doubleValue())
          return "";
        return ""+(number.intValue());
      }

      @Override
      public Number fromString(String s) {
        Number val = Double.parseDouble(s);
        return val.intValue();
      }
    });

    Label enlargedLabel = new Label(label);
    enlargedLabel.setPrefSize(600, 50);
    enlargedLabel.setTranslateX(175);
    enlargedLabel.setTranslateY(10);
    enlargedLabel.getStyleClass().add("enlargedLabel");
    enlargedLabel.setVisible(false);

    CustomAreaChart customAreaChart = new CustomAreaChart(xAxis, yAxis, x, y, width, height, data, enlargedLabel);
    customAreaChart.setLegendVisible(false);

    customAreaChart.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent mouseEvent) {
        background.getChildren().remove(customAreaChart);
        background.getChildren().add(customAreaChart);
        customAreaChart.enlarge(915, 670, 15, 100);
      }
    });

    Label chartLabel = new Label(label);
    chartLabel.setPrefSize(width, 20);
    chartLabel.setTranslateX(x);
    chartLabel.setTranslateY(y - 13);
    chartLabel.getStyleClass().add("chartLabel");

    background.getChildren().addAll(customAreaChart, chartLabel, enlargedLabel);

  }


  private void createAreaChart(int x, int y, int width, int height, HashMap<String, XYChart.Series> dataList, String label) {

    CategoryAxis xAxis = new CategoryAxis();
    xAxis.setTickLabelsVisible(false);
    NumberAxis yAxis = new NumberAxis();
    yAxis.setTickLabelFormatter(new StringConverter<Number>() {
      @Override
      public String toString(Number number) {
        if(number.intValue()!=number.doubleValue())
          return "";
        return ""+(number.intValue());
      }

      @Override
      public Number fromString(String s) {
        Number val = Double.parseDouble(s);
        return val.intValue();
      }
    });

    Label enlargedLabel = new Label(label);
    enlargedLabel.setPrefSize(600, 50);
    enlargedLabel.setTranslateX(175);
    enlargedLabel.setTranslateY(10);
    enlargedLabel.getStyleClass().add("enlargedLabel");
    enlargedLabel.setVisible(false);

    CustomAreaChart customAreaChart = new CustomAreaChart(xAxis, yAxis, x, y, width, height, dataList, enlargedLabel);

    customAreaChart.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent mouseEvent) {
        background.getChildren().remove(customAreaChart);
        background.getChildren().add(customAreaChart);
        customAreaChart.enlarge(915, 670, 15, 100);
      }
    });

    Label chartLabel = new Label(label);
    chartLabel.setPrefSize(width, 20);
    chartLabel.setTranslateX(x);
    chartLabel.setTranslateY(y - 13);
    chartLabel.getStyleClass().add("chartLabel");

    background.getChildren().addAll(customAreaChart, chartLabel, enlargedLabel);

  }


  private void createHandlers() {

  }

}
