package view;

import database.DatabaseManager;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import util.AddOrEditTask;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.IsoFields;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.Locale;

public class CalendarWindowController {

  private Scene scene;
  private Label yearLabel;
  private DatePicker datePicker;
  private Rectangle addRec;
  private Rectangle increaseRec;
  private Rectangle decreaseRec;
  private Label currentWeekLabel;
  private AnchorPane[] dayPanes;
  private AnchorPane[] contentPanes;
  private DatabaseManager databaseManager;

  @FXML
  private AnchorPane background, topBar;


  public void initialize(Scene scene, DatabaseManager databaseManager) {

    this.scene = scene;
    this.databaseManager = databaseManager;

    createCalendar();
    createHandlers();
  }


  private void createCalendar() {

    datePicker = new DatePicker();
    datePicker.setPrefSize(150, 40);
    datePicker.setTranslateX(15);
    datePicker.setTranslateY(15);
    datePicker.setValue(LocalDate.now());
    datePicker.getStyleClass().add("datePicker");

    addRec = new Rectangle(32, 32);
    addRec.setTranslateX(888);
    addRec.setTranslateY(19);
    addRec.getStyleClass().add("addRec");

    yearLabel = new Label(String.valueOf(LocalDate.now().getYear()));
    yearLabel.setPrefSize(200, 50);
    yearLabel.setTranslateX(375);
    yearLabel.setTranslateY(10);
    yearLabel.getStyleClass().add("yearLabel");

    topBar.getChildren().addAll(yearLabel, datePicker, addRec);

    decreaseRec = new Rectangle(16, 16);
    decreaseRec.setTranslateX(375);
    decreaseRec.setTranslateY(118);
    decreaseRec.getStyleClass().add("decreaseRec");

    Label weekLabel = new Label("WEEK");
    weekLabel.setPrefSize(70, 40);
    weekLabel.setTranslateX(417);
    weekLabel.setTranslateY(105);
    weekLabel.getStyleClass().add("weekLabel");

    currentWeekLabel = new Label(String.valueOf(LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(),
        LocalDate.now().getDayOfMonth()).get(WeekFields.of(Locale.GERMANY).weekOfYear())));
    currentWeekLabel.setPrefSize(50, 40);
    currentWeekLabel.setTranslateX(497);
    currentWeekLabel.setTranslateY(105);
    currentWeekLabel.getStyleClass().add("currentWeekLabel");

    increaseRec = new Rectangle(16, 16);
    increaseRec.setTranslateX(555);
    increaseRec.setTranslateY(118);
    increaseRec.getStyleClass().add("increaseRec");

    background.getChildren().addAll(decreaseRec, weekLabel, currentWeekLabel, increaseRec);

    int nrOfDays = 5;
    dayPanes = new AnchorPane[nrOfDays];
    contentPanes = new AnchorPane[nrOfDays];
    for (int i = 0; i < nrOfDays; i++) {
      createWeekDaysLabel(i, 50 + i * 162 + i * 10, 180);
      createDayPane(i, 50 + i * 162 + i * 10, 220);
    }

  }


  private void createWeekDaysLabel(int day, int x, int y) {

    Label dayLabel = new Label();
    dayLabel.setPrefSize(162, 40);
    dayLabel.setTranslateX(x);
    dayLabel.setTranslateY(y);
    dayLabel.getStyleClass().add("weekDayLabel");

    switch (day) {
      case 0:
        dayLabel.setText("MONDAY");
        break;
      case 1:
        dayLabel.setText("TUESDAY");
        break;
      case 2:
        dayLabel.setText("WEDNESDAY");
        break;
      case 3:
        dayLabel.setText("THURSDAY");
        break;
      case 4:
        dayLabel.setText("FRIDAY");
        break;
    }

    background.getChildren().add(dayLabel);

  }


  private void createDayPane(int day, int x, int y) {


    AnchorPane dayPane = new AnchorPane();
    dayPane.setPrefSize(162, 530);
    dayPane.setTranslateX(x);
    dayPane.setTranslateY(y);
    dayPane.getStyleClass().add("dayPane");

    AnchorPane contentPane = new AnchorPane();
    contentPane.setPrefSize(152, 300);
    contentPane.getStyleClass().add("contentPane");
    contentPanes[day] = contentPane;

    ScrollPane scrollPane = new ScrollPane();
    scrollPane.setContent(contentPane);
    scrollPane.setPrefSize(152, 465);
    scrollPane.setTranslateX(5);
    scrollPane.setTranslateY(60);
    scrollPane.getStyleClass().add("scrollPane");
    scrollPane.hbarPolicyProperty().setValue(ScrollPane.ScrollBarPolicy.NEVER);
    scrollPane.vbarPolicyProperty().setValue(ScrollPane.ScrollBarPolicy.AS_NEEDED);

    Label dateLabel = new Label();
    dateLabel.setPrefSize(120, 40);
    dateLabel.setTranslateX(21);
    dateLabel.setTranslateY(10);
    dateLabel.getStyleClass().add("dayLabel");

    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    dateLabel.setText(dtf.format(getDayOfWeek(Integer.parseInt(currentWeekLabel.getText()), day + 1,  Locale.GERMANY)));

    dayPane.getChildren().add(dateLabel);

    dayPanes[day] = dayPane;

    ArrayList<AnchorPane> taskPanes = createTaskPanes(dtf.format(getDayOfWeek(Integer.parseInt(currentWeekLabel.getText()), day + 1,  Locale.GERMANY)));
    contentPane.setPrefHeight(taskPanes.size() * 60 + taskPanes.size() * 8 + 10);
    contentPane.getChildren().addAll(taskPanes);

    dayPane.getChildren().add(scrollPane);

    background.getChildren().addAll(dayPane);

  }


  private LocalDate getDayOfWeek(int weekNumber, int day, Locale locale) {
    return LocalDate
        .of(Integer.parseInt(yearLabel.getText()), 2, 1)
        .with(WeekFields.of(locale).weekOfYear(), weekNumber)
        .with(WeekFields.of(locale).dayOfWeek(), day);
  }


  private ArrayList<AnchorPane> createTaskPanes(String date) {
    ArrayList<AnchorPane> taskPanes = new ArrayList<>();
    ArrayList<String> tasks;
    try {
      tasks = databaseManager.readTaskEntriesByDate(date);
      for (int i = 0; i < tasks.size(); i++) {
        AnchorPane anchorPane = new AnchorPane();
        anchorPane.setPrefSize(152, 60);
        anchorPane.setTranslateX(0);
        anchorPane.setTranslateY(i * 60 + i * 8);
        anchorPane.getStyleClass().add("taskPane");

        String[] s = tasks.get(i).split("§");

        Circle urgencyCircle = new Circle(5);
        urgencyCircle.setTranslateX(15);
        urgencyCircle.setTranslateY(12);
        urgencyCircle.getStyleClass().add(s[1]);

        Label typeLabel = new Label(s[4]);
        typeLabel.setPrefSize(122, 10);
        typeLabel.setTranslateX(27);
        typeLabel.setTranslateY(4);
        typeLabel.getStyleClass().add("taskTypeLabel");

        Label taskLabel = new Label(s[3]);
        taskLabel.setPrefSize(130, 35);
        taskLabel.setTranslateX(10);
        taskLabel.setTranslateY(20);
        taskLabel.getStyleClass().add("taskLabel");

        anchorPane.getChildren().addAll(urgencyCircle, typeLabel, taskLabel);
        taskPanes.add(anchorPane);

      }
    } catch (SQLException e) {
      e.printStackTrace();
    }



    return taskPanes;
  }


  public void update() {

    clear();

    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    for (int i = 0; i < dayPanes.length; i++) {
      Label l = (Label) dayPanes[i].getChildren().get(0);
      l.setText(dtf.format(getDayOfWeek(Integer.parseInt(currentWeekLabel.getText()), i+1,  Locale.GERMANY)));
      ArrayList<AnchorPane> taskPanes = createTaskPanes(dtf.format(getDayOfWeek(Integer.parseInt(currentWeekLabel.getText()), i + 1,  Locale.GERMANY)));
      contentPanes[i].setPrefHeight(taskPanes.size() * 60 + taskPanes.size() * 8);
      contentPanes[i].getChildren().addAll(taskPanes);
    }

  }


  private void clear() {
    for (AnchorPane cp : contentPanes) {
      cp.getChildren().clear();
      cp.setPrefHeight(100);
    }
  }


  private void createHandlers() {

    datePicker.valueProperty().addListener(new ChangeListener<LocalDate>() {
      @Override
      public void changed(ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) {

        int year = newValue.getYear();
        int week = newValue.get(IsoFields.WEEK_OF_WEEK_BASED_YEAR);

        yearLabel.setText(String.valueOf(year));
        currentWeekLabel.setText(String.valueOf(week));
        update();

      }
    });

    addRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        new AddOrEditTask(background, CalendarWindowController.this, databaseManager);
      }
    });

    decreaseRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        int cur = Integer.parseInt(currentWeekLabel.getText());
        if (cur > 1) {
          currentWeekLabel.setText(String.valueOf(cur - 1));
        } else {
          int curYear = Integer.parseInt(yearLabel.getText());
          yearLabel.setText(String.valueOf(curYear-1));
          currentWeekLabel.setText("52");
        }
        update();
      }
    });

    increaseRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        int cur = Integer.parseInt(currentWeekLabel.getText());
        if (cur < 52) {
          currentWeekLabel.setText(String.valueOf(cur + 1));
        } else {
          int curYear = Integer.parseInt(yearLabel.getText());
          yearLabel.setText(String.valueOf(curYear+1));
          currentWeekLabel.setText("1");
        }
        update();
      }
    });

  }

}
