package view;

import database.DatabaseManager;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import util.AddOrEditCandidateInProcess;
import util.DateUtils;
import util.Process;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class CandidateInProcessWindowController {

  private Scene scene;
  private AnchorPane contentPane;
  private Rectangle addRec;
  private DatabaseManager databaseManager;

  @FXML
  private AnchorPane background, topBar;

  @FXML
  private TextField searchTextField;

  @FXML
  private ScrollPane scrollPane;


  public void initialize(Scene scene, DatabaseManager databaseManager) {

    this.scene = scene;
    this.databaseManager = databaseManager;

    createMenuItems();
    createProcesses();
    createHandlers();
  }


  private void createMenuItems() {

    searchTextField.setFocusTraversable(false);

    addRec = new Rectangle(32, 32);
    addRec.setTranslateX(888);
    addRec.setTranslateY(19);
    addRec.getStyleClass().add("addRec");

    topBar.getChildren().add(addRec);

  }


  private void createProcesses() {

    contentPane = new AnchorPane();
    contentPane.setPrefSize(950, 1200);
    contentPane.getStyleClass().add("background");

    scrollPane.setContent(contentPane);

    try {
      updateWindow(databaseManager.readAllCandidatesInProcessEntries());
    } catch (SQLException e) {
      e.printStackTrace();
    }

  }


  public void updateWindow(ArrayList<String> candidates) {

    contentPane.getChildren().clear();

    Collections.sort(candidates, new Comparator<String>() {
      @Override
      public int compare(String o1, String o2) {

        String[] a = o1.split("§");
        String[] b = o2.split("§");

        return a[1].compareTo(b[1]);

      }
    });

    contentPane.setPrefSize(950, candidates.size() * 100 + 10 * candidates.size() + 30);

    for (int i = 0; i < candidates.size(); i++) {
      String[] s = candidates.get(i).split("§");
      Process process = new Process(20, 20 + i * 100 + i * 10, Integer.parseInt(s[0]), s[1], s[2], s[3], s[4], background,
          CandidateInProcessWindowController.this, databaseManager);

      contentPane.getChildren().addAll(process);
    }

    try {
      if (databaseManager.containsCandidatesStatisticsEntry(DateUtils.getTodayDate()) <= 0) {
        databaseManager.createCandidatesStatisticsEntry(DateUtils.getTodayDate(), 0, 0, 0);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

  }


  private void createHandlers() {

    searchTextField.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
      @Override
      public void handle(KeyEvent event) {

        if (event.getCode() == KeyCode.ENTER) {

          if (searchTextField.getText().isEmpty()) {

            try {
              updateWindow(databaseManager.readAllCandidatesInProcessEntries());
            } catch (SQLException e) {
              e.printStackTrace();
            }

          } else {

            try {
              updateWindow(databaseManager.readCandidatesInProcessEntriesFromSearch(searchTextField.getText()));
            } catch (SQLException e) {
              e.printStackTrace();
            }

          }

        }

      }
    });

    addRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        new AddOrEditCandidateInProcess(background, CandidateInProcessWindowController.this, databaseManager);

      }
    });

  }

}
