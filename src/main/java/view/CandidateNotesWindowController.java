package view;

import database.DatabaseManager;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import util.AddOrEditCandidateNote;
import util.AddOrEditJobNote;
import util.Note;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class CandidateNotesWindowController {

  private Scene scene;
  private AnchorPane contentPane;
  private Rectangle addRec;
  private DatabaseManager databaseManager;

  @FXML
  private AnchorPane background, topBar;

  @FXML
  private TextField searchTextField;

  @FXML
  private ScrollPane scrollPane;


  public void initialize(Scene scene, DatabaseManager databaseManager) {

    this.scene = scene;
    this.databaseManager = databaseManager;

    createMenuItems();
    createNotes();
    createHandlers();
  }


  private void createMenuItems() {

    searchTextField.setFocusTraversable(false);

    addRec = new Rectangle(32, 32);
    addRec.setTranslateX(888);
    addRec.setTranslateY(19);
    addRec.getStyleClass().add("addRec");

    topBar.getChildren().add(addRec);

  }


  private void createNotes() {

    contentPane = new AnchorPane();
    contentPane.setPrefSize(950, 1200);
    contentPane.getStyleClass().add("background");

    scrollPane.setContent(contentPane);

    try {
      updateWindow(databaseManager.readAllCandidatesNotesEntries());
    } catch (SQLException e) {
      e.printStackTrace();
    }

  }


  public void updateWindow(ArrayList<String> candidatesNotes) {

    contentPane.getChildren().clear();

    int addOne = candidatesNotes.size() % 3 == 0 ? 0 : 1;
    contentPane.setPrefSize(950, (candidatesNotes.size() / 3 + addOne) * 290 + 10 * candidatesNotes.size() / 3 + 35);

    // Reverse list order to show newest notes first
    Collections.reverse(candidatesNotes);

    int offsetY = 0;
    int offsetX = 0;
    int counter = 0;
    for (int i = 0; i < candidatesNotes.size(); i++) {
      if (i % 3 == 0) {
        offsetY = counter * 300;
        offsetX = 0;
        counter++;
      } else {
        offsetX = i % 3 * 300;
      }

      Note note = null;
      String[] s = candidatesNotes.get(i).split("§");
      if (s[1].isEmpty() && !s[2].isEmpty()) {
        note = new Note(20 + offsetX, 20 + offsetY, Integer.parseInt(s[0]), s[2], s[1], s[3], s[4],
            background, CandidateNotesWindowController.this, databaseManager);
      } else {
        note = new Note(20 + offsetX, 20 + offsetY, Integer.parseInt(s[0]), s[1], s[2], s[3], s[4],
            background, CandidateNotesWindowController.this, databaseManager);
      }

      contentPane.getChildren().addAll(note);
    }

  }


  private void createHandlers() {

    searchTextField.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
      @Override
      public void handle(KeyEvent event) {

        if (event.getCode() == KeyCode.ENTER) {

          if (searchTextField.getText().isEmpty()) {

            try {
              updateWindow(databaseManager.readAllCandidatesNotesEntries());
            } catch (SQLException e) {
              e.printStackTrace();
            }

          } else {

            try {
              updateWindow(databaseManager.readCandidatesNotesEntriesFromSearch(searchTextField.getText()));
            } catch (SQLException e) {
              e.printStackTrace();
            }

          }

        }

      }
    });

    addRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        new AddOrEditCandidateNote(background, CandidateNotesWindowController.this, databaseManager);
      }
    });

  }

}
