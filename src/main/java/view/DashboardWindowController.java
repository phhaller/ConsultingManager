package view;

import database.DatabaseManager;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class DashboardWindowController {

  private Scene scene;
  private DatabaseManager databaseManager;

  @FXML
  private AnchorPane background;

  @FXML
  private ListView tasksListView, companiesListView, jobsListView, candidatesListView;

  @FXML
  private Label dateLabel, dayLabel, activitiesStatsCurrentLabel, activitiesStatsGoalLabel, activitiesStatusLabel;


  public void initialize(Scene scene, DatabaseManager databaseManager) {
    this.scene = scene;
    this.databaseManager = databaseManager;
    createDatePane();
    createTasksPane();
    createCompaniesPane();
    createJobsPane();
    createCandidatesPane();
    createActivitiesPane();
    createHandlers();
  }


  private void createDatePane() {

    LocalDate ld = LocalDate.now();
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.YYYY");
    dateLabel.setText(dtf.format(ld));

    dayLabel.setText(String.valueOf(ld.getDayOfWeek()));

  }


  private void createTasksPane() {

    try {
      ArrayList<String> tasks = databaseManager.readAllTaskEntries();

      Collections.sort(tasks, new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {

          String[] a = o1.split("§");
          String[] b = o2.split("§");

          DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
          return LocalDate.parse(b[2], dtf).compareTo(LocalDate.parse(a[2], dtf));

        }
      });

      for (String s : tasks) {
        String[] a = s.split("§");
        tasksListView.getItems().add(a[2] + " - " + a[3]);
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

  }


  private void createCompaniesPane() {

    try {
      ArrayList<String> companies = databaseManager.readAllCompaniesEntries();

      Collections.sort(companies, new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {

          String[] a = o1.split("§");
          String[] b = o2.split("§");

          DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
          return LocalDate.parse(b[3], dtf).compareTo(LocalDate.parse(a[3], dtf));

        }
      });

      for (String s : companies) {
        String[] a = s.split("§");
        companiesListView.getItems().add(a[1]);
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

  }


  private void createJobsPane() {

    try {
      ArrayList<String> jobs = databaseManager.readAllJobsEntries();

      Collections.sort(jobs, new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {

          String[] a = o1.split("§");
          String[] b = o2.split("§");

          DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
          return LocalDate.parse(a[4], dtf).compareTo(LocalDate.parse(b[4], dtf));

        }
      }.reversed());

      for (String s : jobs) {
        String[] a = s.split("§");
        jobsListView.getItems().add(a[1] + " - " + a[2]);
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

  }


  private void createCandidatesPane() {

    try {
      ArrayList<String> candidates = databaseManager.readAllCandidatesInProcessEntries();

      Collections.sort(candidates, new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {

          String[] a = o1.split("§");
          String[] b = o2.split("§");

          DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
          return LocalDate.parse(a[5], dtf).compareTo(LocalDate.parse(b[5], dtf));

        }
      }.reversed());

      for (String s : candidates) {
        String[] a = s.split("§");
        if (a[4].equals("CV sent") || a[4].equals("Interview")) {
          candidatesListView.getItems().add(a[1] + " - " + a[2] + " - " + a[3]);
        }
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

  }


  private void createActivitiesPane() {

    try {

      ArrayList<String> activities = databaseManager.readAllActivitiesEntries();

      int total = activities.size();
      int completed = 0;

      for (String activity : activities) {

        String[] a = activity.split("§");
        if (Integer.parseInt(a[4]) >= Integer.parseInt(a[2])) {
          completed += 1;
        }

      }

      activitiesStatsCurrentLabel.setText(String.valueOf(completed));
      activitiesStatsGoalLabel.setText(String.valueOf(total));

      int mid = (int) Math.ceil(total / 2.0);

      if (completed < mid) {
        if (completed <= 0) {
          activitiesStatusLabel.setText("Nothing completed, let's get crackin!");
        } else {
          activitiesStatusLabel.setText("You're progressing, keep it up!");
        }
      }

      if (completed == mid) {
        activitiesStatusLabel.setText("You're halfway there, keep going!");
      }

      if (completed > mid) {
        if (completed >= total) {
          activitiesStatusLabel.setText("All done, great job!");
        } else {
          activitiesStatusLabel.setText("Only " + (total - completed) + " left uncompleted, you're almost there!");
        }
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

  }


  private void createHandlers() {

    tasksListView.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        System.out.println("util.Task clicked " + tasksListView.getSelectionModel().getSelectedItem());
      }
    });


  }

}
