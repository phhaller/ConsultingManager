package view;

import database.DatabaseManager;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import util.Activity;
import util.AddOrEditActivity;
import util.DateUtils;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class ActivityWindowController {

  private Scene scene;
  private AnchorPane contentPane;
  private Rectangle addRec;
  private DatabaseManager databaseManager;

  @FXML
  private AnchorPane background, topBar;

  @FXML
  private TextField searchTextField;

  @FXML
  private ScrollPane scrollPane;


  public void initialize(Scene scene, DatabaseManager databaseManager) {

    this.scene = scene;
    this.databaseManager = databaseManager;

    createActivities();
    createHandlers();

  }


  private void createActivities() {

    searchTextField.setFocusTraversable(false);

    addRec = new Rectangle(32, 32);
    addRec.setTranslateX(888);
    addRec.setTranslateY(19);
    addRec.getStyleClass().add("addRec");

    topBar.getChildren().add(addRec);

    contentPane = new AnchorPane();
    contentPane.setPrefSize(950, 1200);
    contentPane.getStyleClass().add("background");

    scrollPane.setContent(contentPane);

    try {
      updateWindow(databaseManager.readAllActivitiesEntries());
    } catch (SQLException e) {
      e.printStackTrace();
    }

  }

  public void updateWindow(ArrayList<String> activities) {

    contentPane.getChildren().clear();

    contentPane.setPrefSize(950, activities.size() * 100 + 10 * activities.size() + 30);

    for (int i = 0; i < activities.size(); i++) {
      String[] s = activities.get(i).split("§");
      String[] dateInfo = checkCurrentDateNeedsUpdate(Integer.parseInt(s[0]), s[3], s[4], s[5]);
      Activity activity = new Activity(20, 20 + i * 100 + i * 10, Integer.parseInt(s[0]), s[1],
          Integer.parseInt(s[2]), s[3], Integer.parseInt(dateInfo[0]), dateInfo[1], background,
          ActivityWindowController.this, databaseManager);

      try {
        if (databaseManager.containsActivitiesStatisticsEntry(DateUtils.getTodayDate(), Integer.parseInt(s[0])) <= 0) {
          databaseManager.createActivitiesStatisticsEntry(DateUtils.getTodayDate(), Integer.parseInt(s[0]), 0);
        }
      } catch (SQLException e) {
        e.printStackTrace();
      }

      contentPane.getChildren().addAll(activity);
    }

  }


  private String[] checkCurrentDateNeedsUpdate(int id, String unit, String current, String date) {

    String[] update = new String[2];
    String cur = current;
    String curDate = date;
    LocalDate now = LocalDate.now();

    switch (unit) {
      case "day":
        if (!("d_" + DateUtils.formatDate(now)).equals(date)) {
          cur = "0";
          curDate = "d_" + DateUtils.formatDate(now);
        }
        break;
      case "week":
        if (!("w_" + DateUtils.getWeekFromDate(now)).equals(date)) {
          cur = "0";
          curDate = "w_" + DateUtils.getWeekFromDate(now);
        }
        break;
      case "month":
        if (!("m_" + DateUtils.getMonthNumberFromDate(now)).equals(date)) {
          cur = "0";
          curDate = "m_" + DateUtils.getMonthNumberFromDate(now);
        }
        break;
      case "year":
        if (!("y_" + DateUtils.getYearFromDate(now)).equals(date)) {
          cur = "0";
          curDate = "y_" + DateUtils.getYearFromDate(now);
        }
        break;
    }

    try {
      if (!curDate.equals(date)) {
        databaseManager.updateActivitiesEntryCurrent(id, Integer.parseInt(cur));
        databaseManager.updateActivitiesEntryCurrentDate(id, curDate);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    update[0] = cur;
    update[1] = curDate;

    return  update;

  }


  private void createHandlers() {

    searchTextField.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
      @Override
      public void handle(KeyEvent event) {

        if (event.getCode() == KeyCode.ENTER) {

          if (searchTextField.getText().isEmpty()) {

            try {
              updateWindow(databaseManager.readAllActivitiesEntries());
            } catch (SQLException e) {
              e.printStackTrace();
            }

          } else {

            try {
              updateWindow(databaseManager.readActivitiesEntriesFromSearch(searchTextField.getText()));
            } catch (SQLException e) {
              e.printStackTrace();
            }

          }

        }

      }
    });

    addRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        new AddOrEditActivity(background, ActivityWindowController.this, databaseManager);
      }
    });

  }

}
