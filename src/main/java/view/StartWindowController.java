package view;

import database.DatabaseManager;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import util.Activity;
import util.DateUtils;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class StartWindowController {

  private Scene scene;
  private String contentType;
  private AnchorPane contentPane;
  private ArrayList<AnchorPane> menuItems;
  private DatabaseManager databaseManager;

  @FXML
  private AnchorPane background;

  @FXML
  private Label homeLabel;

  @FXML
  private AnchorPane tasks, calendar, activity, companies, jobs, jobsNotes, candidateAll, candidateInProcess,
    candidateNotes, statistics, settings;



  private void initialize() {

    databaseManager = new DatabaseManager();

    loadPane("dashboardWindow");
    contentType = "dashboard";
    menuItems = new ArrayList<>();
    menuItems.add(tasks);
    menuItems.add(calendar);
    menuItems.add(activity);
    menuItems.add(companies);
    menuItems.add(jobs);
    menuItems.add(jobsNotes);
    menuItems.add(candidateAll);
    menuItems.add(candidateInProcess);
    menuItems.add(candidateNotes);
    menuItems.add(statistics);
    menuItems.add(settings);
    createHandlers();

  }


  private void createHandlers() {

    homeLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        if (!contentType.equals("dashboard")) {
          clearActive();
          loadPane("dashboardWindow");
          contentType = "dashboard";
        }
      }
    });

    tasks.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        if (!contentType.equals("tasks")) {
          clearActive();
          loadPane("tasksWindow");
          contentType = "tasks";
          tasks.getStyleClass().add("menuActive");
        }
      }
    });


    calendar.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        if (!contentType.equals("calendar")) {
          clearActive();
          loadPane("calendarWindow");
          contentType = "calendar";
          calendar.getStyleClass().add("menuActive");
        }
      }
    });


    activity.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        if (!contentType.equals("activity")) {
          clearActive();
          loadPane("activityWindow");
          contentType = "activity";
          activity.getStyleClass().add("menuActive");
        }
      }
    });

    companies.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        if (!contentType.equals("companies")) {
          clearActive();
          loadPane("companiesWindow");
          contentType = "companies";
          companies.getStyleClass().add("menuActive");
        }
      }
    });

    jobs.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        if (!contentType.equals("jobs")) {
          clearActive();
          loadPane("jobsWindow");
          contentType = "jobs";
          jobs.getStyleClass().add("menuActive");
        }
      }
    });

    jobsNotes.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        if (!contentType.equals("jobsNotes")) {
          clearActive();
          loadPane("jobsNotesWindow");
          contentType = "jobsNotes";
          jobsNotes.getStyleClass().add("menuActive");
        }
      }
    });

    candidateAll.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        if (!contentType.equals("candidateAll")) {
          clearActive();
          loadPane("candidateAllWindow");
          contentType = "candidateAll";
          candidateAll.getStyleClass().add("menuActive");
        }
      }
    });

    candidateInProcess.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        if (!contentType.equals("candidateInProcess")) {
          clearActive();
          loadPane("candidateInProcessWindow");
          contentType = "candidateInProcess";
          candidateInProcess.getStyleClass().add("menuActive");
        }
      }
    });

    candidateNotes.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        if (!contentType.equals("candidateNotes")) {
          clearActive();
          loadPane("candidateNotesWindow");
          contentType = "candidateNotes";
          candidateNotes.getStyleClass().add("menuActive");
        }
      }
    });

    statistics.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        if (!contentType.equals("statistics")) {
          clearActive();
          loadPane("statisticsWindow");
          contentType = "statistics";
          statistics.getStyleClass().add("menuActive");
        }
      }
    });

    settings.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        if (!contentType.equals("settings")) {
          clearActive();
          loadPane("settingsWindow");
          contentType = "settings";
          settings.getStyleClass().add("menuActive");
        }
      }
    });

  }


  private void clearActive() {

    for (AnchorPane p : menuItems) {
      p.getStyleClass().remove("menuActive");
    }

  }


  private void loadPane(String fxml) {

    if (contentPane != null) {
      background.getChildren().remove(contentPane);
    }

    try {
      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/" + fxml + ".fxml"));
      AnchorPane pane = loader.load();
      contentPane = pane;

      switch (fxml) {
        case "dashboardWindow":
          DashboardWindowController dashboardWindowController = loader.getController();
          dashboardWindowController.initialize(scene, databaseManager);
          break;
        case "tasksWindow":
          TasksWindowController tasksWindowController = loader.getController();
          tasksWindowController.initialize(scene, databaseManager);
          break;
        case "calendarWindow":
          CalendarWindowController calendarWindowController = loader.getController();
          calendarWindowController.initialize(scene, databaseManager);
          break;
        case "activityWindow":
          ActivityWindowController activityWindowController = loader.getController();
          activityWindowController.initialize(scene, databaseManager);
          break;
        case "companiesWindow":
          CompaniesWindowController companiesWindowController = loader.getController();
          companiesWindowController.initialize(scene, databaseManager);
          break;
        case "jobsWindow":
          JobsWindowController jobsWindowController = loader.getController();
          jobsWindowController.initialize(scene, databaseManager);
          break;
        case "jobsNotesWindow":
          JobsNotesWindowController jobsNotesWindowController = loader.getController();
          jobsNotesWindowController.initialize(scene, databaseManager);
          break;
        case "candidateAllWindow":
          CandidateAllWindowController candidateAllWindowController = loader.getController();
          candidateAllWindowController.initialize(scene, databaseManager);
          break;
        case "candidateInProcessWindow":
          CandidateInProcessWindowController candidateInProcessWindowController = loader.getController();
          candidateInProcessWindowController.initialize(scene, databaseManager);
          break;
        case "candidateNotesWindow":
          CandidateNotesWindowController candidateNotesWindowController = loader.getController();
          candidateNotesWindowController.initialize(scene, databaseManager);
          break;
        case "statisticsWindow":
          StatisticsWindowController statisticsWindowController = loader.getController();
          statisticsWindowController.initialize(scene, databaseManager);
          break;
        case "settingsWindow":
          SettingsWindowController settingsWindowController = loader.getController();
          settingsWindowController.setScene(scene);
          break;
      }

      pane.setTranslateX(250);
      pane.setTranslateY(0);
      background.getChildren().add(pane);

    } catch (IOException e) {
      e.printStackTrace();
    }

  }



  public void setScene(Scene scene) {
    this.scene = scene;
    initialize();
  }

}
