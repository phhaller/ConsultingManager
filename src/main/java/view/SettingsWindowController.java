package view;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

public class SettingsWindowController {

  private Scene scene;

  @FXML
  private AnchorPane background;


  private void initialize() {
    createHandlers();
  }


  private void createHandlers() {

    background.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        System.out.println("Settings Window Clicked!");
      }
    });

  }






  public void setScene(Scene scene) {
    this.scene = scene;
    initialize();
  }

}
