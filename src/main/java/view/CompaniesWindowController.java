package view;

import database.DatabaseManager;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import util.*;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class CompaniesWindowController {

  private Scene scene;
  private AnchorPane contentPane;
  private Rectangle addRec;
  private DatabaseManager databaseManager;

  @FXML
  private AnchorPane background, topBar;

  @FXML
  private TextField searchTextField;

  @FXML
  private ScrollPane scrollPane;


  public void initialize(Scene scene, DatabaseManager databaseManager) {

    this.scene = scene;
    this.databaseManager = databaseManager;

    createMenuItems();
    createJobs();
    createHandlers();
  }


  private void createMenuItems() {

    searchTextField.setFocusTraversable(false);

    addRec = new Rectangle(32, 32);
    addRec.setTranslateX(888);
    addRec.setTranslateY(19);
    addRec.getStyleClass().add("addRec");

    topBar.getChildren().add(addRec);

  }


  private void createJobs() {

    contentPane = new AnchorPane();
    contentPane.setPrefSize(950, 1200);
    contentPane.getStyleClass().add("background");

    scrollPane.setContent(contentPane);

    try {
      updateWindow(databaseManager.readAllCompaniesEntries());
    } catch (SQLException e) {
      e.printStackTrace();
    }

  }


  public void updateWindow(ArrayList<String> companies) {

    contentPane.getChildren().clear();

    Collections.sort(companies, new Comparator<String>() {
      @Override
      public int compare(String o1, String o2) {

        String[] a = o1.split("§");
        String[] b = o2.split("§");

        return a[1].compareTo(b[1]);

      }
    });

    contentPane.setPrefSize(950, companies.size() * 100 + 10 * companies.size() + 30);

    for (int i = 0; i < companies.size(); i++) {
      String[] s = companies.get(i).split("§");
      Company company = new Company(20, 20 + i * 100 + i * 10, Integer.parseInt(s[0]), s[1], background,
          CompaniesWindowController.this, databaseManager);

      contentPane.getChildren().addAll(company);
    }

  }


  private void createHandlers() {

    searchTextField.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
      @Override
      public void handle(KeyEvent event) {

        if (event.getCode() == KeyCode.ENTER) {

          if (searchTextField.getText().isEmpty()) {

            try {
              updateWindow(databaseManager.readAllCompaniesEntries());
            } catch (SQLException e) {
              e.printStackTrace();
            }

          } else {

            try {
              updateWindow(databaseManager.readCompaniesEntriesFromSearch(searchTextField.getText()));
            } catch (SQLException e) {
              e.printStackTrace();
            }

          }

        }

      }
    });

    addRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        new AddOrEditCompany(background, CompaniesWindowController.this, databaseManager);
      }
    });

  }

}
