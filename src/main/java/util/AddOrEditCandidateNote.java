package util;

import database.DatabaseManager;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import view.CandidateNotesWindowController;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class AddOrEditCandidateNote {


  private AnchorPane background;
  private CandidateNotesWindowController candidateNotesWindowController;
  private DatabaseManager databaseManager;

  private AnchorPane addOrEditPane;
  private Rectangle backRec;
  private ComboBox candidateChooser;
  private ComboBox jobsChooser;
  private TextArea noteTextArea;
  private Label saveLabel;

  private boolean editing = false;
  private int taskID;


  public AddOrEditCandidateNote(AnchorPane background, CandidateNotesWindowController windowController, DatabaseManager databaseManager) {

    this.background = background;
    this.candidateNotesWindowController = windowController;
    this.databaseManager = databaseManager;
    createWindow();
    createHandlers();

  }


  private void createWindow() {

    addOrEditPane = new AnchorPane();
    addOrEditPane.setPrefSize(950, 800);
    addOrEditPane.getStyleClass().add("addOrEditBackground");

    backRec = new Rectangle(20, 20);
    backRec.setTranslateX(25);
    backRec.setTranslateY(25);
    backRec.getStyleClass().add("backRec");

    AnchorPane formPane = new AnchorPane();
    formPane.setPrefSize(600, 555);
    formPane.setTranslateX(175);
    formPane.setTranslateY(157);
    formPane.getStylesheets().clear();
    formPane.getStylesheets().add("/css/addOrEditStylesheet.css");
    formPane.getStyleClass().add("formPane");

    Label companyLabel = new Label("Candidate");
    companyLabel.setTranslateX(50);
    companyLabel.setTranslateY(40);
    companyLabel.getStyleClass().add("formLabel");

    candidateChooser = new ComboBox();
    candidateChooser.setPrefSize(500, 40);
    candidateChooser.setTranslateX(50);
    candidateChooser.setTranslateY(60);

    try {
      addItemsToChooser(candidateChooser, databaseManager.readAllCandidatesAllEntries());
    } catch (SQLException e) {
      e.printStackTrace();
    }

    Label jobTitleLabel = new Label("Job Title");
    jobTitleLabel.setTranslateX(50);
    jobTitleLabel.setTranslateY(130);
    jobTitleLabel.getStyleClass().add("formLabel");

    jobsChooser = new ComboBox();
    jobsChooser.setPrefSize(500, 40);
    jobsChooser.setTranslateX(50);
    jobsChooser.setTranslateY(150);

    try {
      addItemsToChooser(jobsChooser, databaseManager.readAllJobsEntries());
    } catch (SQLException e) {
      e.printStackTrace();
    }

    Label noteLabel = new Label("Note");
    noteLabel.setTranslateX(50);
    noteLabel.setTranslateY(220);
    noteLabel.getStyleClass().add("formLabel");

    noteTextArea = new TextArea();
    noteTextArea.setPrefSize(500, 200);
    noteTextArea.setTranslateX(50);
    noteTextArea.setTranslateY(240);

    saveLabel = new Label("Save");
    saveLabel.setPrefSize(240, 40);
    saveLabel.setTranslateX(50);
    saveLabel.setTranslateY(470);
    saveLabel.getStyleClass().add("saveLabel");

    formPane.getChildren().addAll(companyLabel, candidateChooser, jobTitleLabel, jobsChooser, noteLabel, noteTextArea, saveLabel);

    addOrEditPane.getChildren().addAll(backRec, formPane);

    background.getChildren().add(addOrEditPane);

  }


  private void addItemsToChooser(ComboBox chooser, ArrayList<String> list) {

    chooser.getItems().clear();

    Collections.sort(list, new Comparator<String>() {
      @Override
      public int compare(String o1, String o2) {

        String[] a = o1.split("§");
        String[] b = o2.split("§");

        return a[2].compareTo(b[2]);

      }
    });

    chooser.getItems().add("");

    for (String c : list) {
      String[] s = c.split("§");
      if (chooser == jobsChooser) {
        if (s[6].equals("open")) {
          chooser.getItems().add(s[2] + " - " + s[1]);
        }
      } else {
        chooser.getItems().add(s[1] + " " + s[2]);
      }
    }

    chooser.getSelectionModel().select("");

  }


  public void edit(int taskID, String name, String job, String note) {

    editing = true;
    this.taskID = taskID;
    candidateChooser.getSelectionModel().select(name);
    jobsChooser.getSelectionModel().select(job);
    noteTextArea.setText(note);

  }


  private void createHandlers() {

    backRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        background.getChildren().remove(addOrEditPane);
      }
    });

    saveLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        if (!editing) {

          try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            databaseManager.createCandidatesNotesEntry(
                candidateChooser.getValue().toString().replace("'", "''"),
                jobsChooser.getValue().toString().replace("'", "''"),
                noteTextArea.getText().replace("'", "''"),
                dtf.format(LocalDate.now()));
            background.getChildren().remove(addOrEditPane);
            candidateNotesWindowController.updateWindow(databaseManager.readAllCandidatesNotesEntries());
          } catch (SQLException e) {
            e.printStackTrace();
          }

        } else {

          try {
            databaseManager.updateCandidatesNotesEntryAfterEdit(
                taskID,
                candidateChooser.getValue().toString().replace("'", "''"),
                jobsChooser.getValue().toString().replace("'", "''"),
                noteTextArea.getText().replace("'", "''"));
            background.getChildren().remove(addOrEditPane);
            candidateNotesWindowController.updateWindow(databaseManager.readAllCandidatesNotesEntries());
          } catch (SQLException e) {
            e.printStackTrace();
          }

        }
      }
    });

  }




}
