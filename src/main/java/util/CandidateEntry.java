package util;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;

public class CandidateEntry {

  private AnchorPane background;
  private String candidateDetails;
  private ArrayList<String> jobsDetails;

  private AnchorPane addOrEditPane;
  private Rectangle backRec;

  private Label firstName;
  private Label lastName;
  private Label jobTitle;
  private Label salary;
  private ListView processes;
  private Label cvsSent;
  private Label interviews;
  private Label placements;


  public CandidateEntry(AnchorPane background, String candidateDetails, ArrayList<String> jobsDetails) {

    this.background = background;
    this.candidateDetails = candidateDetails;
    this.jobsDetails = jobsDetails;
    createWindow();
    createHandlers();

  }


  private void createWindow() {

    // id, companyName, lastModified
    String[] c = candidateDetails.split("§");

    addOrEditPane = new AnchorPane();
    addOrEditPane.setPrefSize(950, 800);
    addOrEditPane.getStyleClass().add("addOrEditBackground");

    backRec = new Rectangle(20, 20);
    backRec.setTranslateX(25);
    backRec.setTranslateY(25);
    backRec.getStyleClass().add("backRec");

    AnchorPane formPane = new AnchorPane();
    formPane.setPrefSize(600, 530);
    formPane.setTranslateX(175);
    formPane.setTranslateY(170);
    formPane.getStylesheets().clear();
    formPane.getStylesheets().add("/css/addOrEditStylesheet.css");
    formPane.getStyleClass().add("formPane");

    Label firstNameLabel = new Label("First Name");
    firstNameLabel.setTranslateX(50);
    firstNameLabel.setTranslateY(40);
    firstNameLabel.getStyleClass().add("formLabel");

    firstName = new Label(c[1]);
    firstName.setTranslateX(50);
    firstName.setTranslateY(65);
    firstName.setMaxWidth(250);
    firstName.getStyleClass().add("formText");

    Label lastNameLabel = new Label("Last Name");
    lastNameLabel.setTranslateX(340);
    lastNameLabel.setTranslateY(40);
    lastNameLabel.getStyleClass().add("formLabel");

    lastName = new Label(c[2]);
    lastName.setTranslateX(340);
    lastName.setTranslateY(65);
    lastName.setMaxWidth(250);
    lastName.getStyleClass().add("formText");

    Line divider1 = new Line(30, 105, 540, 105);
    divider1.getStyleClass().add("dividerLine");


    Label jobTitleLabel = new Label("Job Title");
    jobTitleLabel.setTranslateX(50);
    jobTitleLabel.setTranslateY(130);
    jobTitleLabel.getStyleClass().add("formLabel");

    jobTitle = new Label(c[3]);
    jobTitle.setTranslateX(50);
    jobTitle.setTranslateY(155);
    jobTitle.setMaxWidth(250);
    jobTitle.getStyleClass().add("formText");

    Label salaryLabel = new Label("Salary");
    salaryLabel.setTranslateX(340);
    salaryLabel.setTranslateY(130);
    salaryLabel.getStyleClass().add("formLabel");

    salary = new Label(c[4]);
    salary.setTranslateX(340);
    salary.setTranslateY(155);
    salary.getStyleClass().add("formText");

    Line divider2 = new Line(30, 195, 540, 195);
    divider2.getStyleClass().add("dividerLine");

    Label processesLabel = new Label("Processes");
    processesLabel.setTranslateX(50);
    processesLabel.setTranslateY(220);
    processesLabel.getStyleClass().add("formLabel");

    processes = new ListView();
    processes.setPrefSize(500, 105);
    processes.setTranslateX(50);
    processes.setTranslateY(245);
    processes.getStyleClass().add("listView");

    for (String job : jobsDetails) {
      String[] j = job.split("§");
      processes.getItems().add(j[3] + " - " + j[2]);
    }

    Line divider3 = new Line(30, 375, 540, 375);
    divider3.getStyleClass().add("dividerLine");

    Label cvsSentLabel = new Label("CVs sent");
    cvsSentLabel.setTranslateX(50);
    cvsSentLabel.setTranslateY(400);
    cvsSentLabel.getStyleClass().add("formLabel");

    cvsSent = new Label(c[6]);
    cvsSent.setTranslateX(170);
    cvsSent.setTranslateY(400);
    cvsSent.getStyleClass().add("formText");


    Label interviewsLabel = new Label("Interviews");
    interviewsLabel.setTranslateX(50);
    interviewsLabel.setTranslateY(430);
    interviewsLabel.getStyleClass().add("formLabel");

    interviews = new Label(c[7]);
    interviews.setTranslateX(170);
    interviews.setTranslateY(430);
    interviews.getStyleClass().add("formText");


    Label placementsLabel = new Label("Placements");
    placementsLabel.setTranslateX(50);
    placementsLabel.setTranslateY(460);
    placementsLabel.getStyleClass().add("formLabel");

    placements = new Label(c[8]);
    placements.setTranslateX(170);
    placements.setTranslateY(460);
    placements.getStyleClass().add("formText");


    formPane.getChildren().addAll(firstNameLabel, firstName, lastNameLabel, lastName, divider1,
        jobTitleLabel, jobTitle, salaryLabel, salary, divider2, processesLabel, processes, divider3,
        cvsSentLabel, cvsSent, interviewsLabel, interviews, placementsLabel, placements);


    addOrEditPane.getChildren().addAll(backRec, formPane);

    background.getChildren().add(addOrEditPane);

  }


  private void createHandlers() {

    backRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        background.getChildren().remove(addOrEditPane);
      }
    });

  }

}