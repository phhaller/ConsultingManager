package util;

import database.DatabaseManager;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import view.JobsWindowController;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

public class Job extends AnchorPane {

  private int x;
  private int y;
  private int id;
  private String jobTitle;
  private String company;
  private String urgency;
  private String dateAdded;
  private String salary;
  private String status;
  private String link;
  private Rectangle goToRec;
  private Rectangle editRec;
  private Rectangle deleteRec;
  private AnchorPane rootPane;
  private JobsWindowController jobsWindowController;
  private DatabaseManager databaseManager;

  public Job(int x, int y, int id, String jobTitle, String company, String urgency, String dateAdded, String salary,
             String status, String link, AnchorPane rootPane, JobsWindowController jobsWindowController,
             DatabaseManager databaseManager) {
    this.x = x;
    this.y = y;
    this.id = id;
    this.jobTitle = jobTitle;
    this.company = company;
    this.urgency = urgency;
    this.dateAdded = dateAdded;
    this.salary = salary;
    this.status = status;
    this.link = link;
    this.rootPane = rootPane;
    this.jobsWindowController = jobsWindowController;
    this.databaseManager = databaseManager;

    createJob();
    createHandlers();
  }


  private void createJob() {

    setPrefSize(900, 100);
    setTranslateX(x);
    setTranslateY(y);
    getStyleClass().add("jobPane");

    Label jobTitleLabel = new Label(jobTitle);
    jobTitleLabel.setPrefSize(300, 50);
    jobTitleLabel.setTranslateX(30);
    jobTitleLabel.setTranslateY(25);
    jobTitleLabel.getStyleClass().add("jobTitleLabel");

    Line divider1 = new Line(340, 20, 340, 80);
    divider1.getStyleClass().add("dividerLine");

    Label companyLabel = new Label(company);
    companyLabel.setPrefSize(200, 50);
    companyLabel.setTranslateX(350);
    companyLabel.setTranslateY(25);
    companyLabel.getStyleClass().add("companyLabel");

    Line divider2 = new Line(560, 20, 560, 80);
    divider2.getStyleClass().add("dividerLine");

    Label statusLabel = new Label(status);
    statusLabel.setPrefSize(100, 50);
    statusLabel.setTranslateX(570);
    statusLabel.setTranslateY(25);
    statusLabel.getStyleClass().add("statusLabel");

    Line divider3 = new Line(680, 20, 680, 80);
    divider3.getStyleClass().add("dividerLine");

    Label dateAddedLabel = new Label(dateAdded);
    dateAddedLabel.setPrefSize(100, 50);
    dateAddedLabel.setTranslateX(690);
    dateAddedLabel.setTranslateY(25);
    dateAddedLabel.getStyleClass().add("statusLabel");

    Line divider4 = new Line(800, 20, 800, 80);
    divider4.getStyleClass().add("dividerLine");

    goToRec = new Rectangle(15, 15);
    goToRec.setTranslateX(818);
    goToRec.setTranslateY(40);
    goToRec.getStyleClass().add("goToRec");
    goToRec.setVisible(false);

    Line divider5 = new Line(850, 20, 850, 80);
    divider5.getStyleClass().add("dividerLine");

    editRec = new Rectangle(20, 20);
    editRec.setTranslateX(865);
    editRec.setTranslateY(25);
    editRec.getStyleClass().add("editRec");
    editRec.setVisible(false);

    deleteRec = new Rectangle(20, 20);
    deleteRec.setTranslateX(865);
    deleteRec.setTranslateY(55);
    deleteRec.getStyleClass().add("deleteRec");
    deleteRec.setVisible(false);

    getChildren().addAll(jobTitleLabel, divider1, companyLabel, divider2, statusLabel, divider3, dateAddedLabel,
        divider4, goToRec, divider5, editRec, deleteRec);

  }


  private void createHandlers() {

    addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        goToRec.setVisible(true);
        editRec.setVisible(true);
        deleteRec.setVisible(true);
      }
    });

    addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        goToRec.setVisible(false);
        editRec.setVisible(false);
        deleteRec.setVisible(false);
      }
    });

    goToRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        try {
          String jobDetails = databaseManager.readJobByID(id);
          new JobEntry(rootPane, jobDetails);
        } catch (SQLException e) {
          e.printStackTrace();
        }

      }
    });

    editRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        AddOrEditJob addOrEditJob = new AddOrEditJob(rootPane, jobsWindowController, databaseManager);
        addOrEditJob.edit(id, jobTitle, company, urgency, salary, link, status);

      }
    });

    deleteRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        try {
          databaseManager.deleteJobsEntry(id);
          jobsWindowController.updateWindow(databaseManager.readAllJobsEntries());
        } catch (SQLException e) {
          e.printStackTrace();
        }

      }
    });

  }


  public int getTaskId() {
    return id;
  }

  public String getJobTitle() {
    return jobTitle;
  }

  public String getCompany() {
    return company;
  }

  public String getStatus() {
    return status;
  }

  public String getDateAdded() {
    return dateAdded;
  }

}
