package util;

import database.DatabaseManager;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import view.CandidateInProcessWindowController;

import java.sql.SQLException;

public class Process extends AnchorPane {

  private int x;
  private int y;
  private int id;
  private String name;
  private String job;
  private String company;
  private String status;
  private Rectangle editRec;
  private Rectangle deleteRec;
  private AnchorPane rootPane;
  private CandidateInProcessWindowController candidateInProcessWindowController;
  private DatabaseManager databaseManager;

  public Process(int x, int y, int id, String name, String job, String company, String status,
                   AnchorPane rootPane, CandidateInProcessWindowController candidateInProcessWindowController, DatabaseManager databaseManager) {
    this.x = x;
    this.y = y;
    this.id = id;
    this.name = name;
    this.job = job;
    this.company = company;
    this.status = status;
    this.rootPane = rootPane;
    this.candidateInProcessWindowController = candidateInProcessWindowController;
    this.databaseManager = databaseManager;

    createJob();
    createHandlers();
  }


  private void createJob() {

    setPrefSize(900, 100);
    setTranslateX(x);
    setTranslateY(y);
    getStyleClass().add("candidatePane");

    Label nameLabel = new Label(name);
    nameLabel.setPrefSize(200, 50);
    nameLabel.setTranslateX(30);
    nameLabel.setTranslateY(25);
    nameLabel.getStyleClass().add("candidateNameLabel");

    Line divider1 = new Line(240, 20, 240, 80);
    divider1.getStyleClass().add("dividerLine");

    Label jobLabel = new Label(job);
    jobLabel.setPrefSize(250, 50);
    jobLabel.setTranslateX(250);
    jobLabel.setTranslateY(25);
    jobLabel.getStyleClass().add("candidateJobLabel");

    Line divider2 = new Line(510, 20, 510, 80);
    divider2.getStyleClass().add("dividerLine");

    Label companyLabel = new Label(company);
    companyLabel.setPrefSize(200, 50);
    companyLabel.setTranslateX(520);
    companyLabel.setTranslateY(25);
    companyLabel.getStyleClass().add("jobCompanyLabel");

    Line divider3 = new Line(730, 20, 730, 80);
    divider3.getStyleClass().add("dividerLine");

    Label statusLabel = new Label(status);
    statusLabel.setPrefSize(100, 50);
    statusLabel.setTranslateX(740);
    statusLabel.setTranslateY(25);
    statusLabel.getStyleClass().add("statusLabel");

    Line divider4 = new Line(850, 20, 850, 80);
    divider4.getStyleClass().add("dividerLine");

    editRec = new Rectangle(20, 20);
    editRec.setTranslateX(865);
    editRec.setTranslateY(25);
    editRec.getStyleClass().add("editRec");
    editRec.setVisible(false);

    deleteRec = new Rectangle(20, 20);
    deleteRec.setTranslateX(865);
    deleteRec.setTranslateY(55);
    deleteRec.getStyleClass().add("deleteRec");
    deleteRec.setVisible(false);

    getChildren().addAll(nameLabel, divider1, jobLabel, divider2, companyLabel, divider3, statusLabel, divider4,
        editRec, deleteRec);

  }


  private void createHandlers() {

    addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        editRec.setVisible(true);
        deleteRec.setVisible(true);
      }
    });

    addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        editRec.setVisible(false);
        deleteRec.setVisible(false);
      }
    });

    editRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        AddOrEditCandidateInProcess addOrEditCandidateInProcess = new AddOrEditCandidateInProcess(rootPane, candidateInProcessWindowController, databaseManager);
        addOrEditCandidateInProcess.edit(id, name, company, job, status);

      }
    });

    deleteRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        try {
          databaseManager.deleteCandidatesInProcessEntry(id);
          candidateInProcessWindowController.updateWindow(databaseManager.readAllCandidatesInProcessEntries());
        } catch (SQLException e) {
          e.printStackTrace();
        }

      }
    });

  }


  public int getTaskId() {
    return id;
  }


}
