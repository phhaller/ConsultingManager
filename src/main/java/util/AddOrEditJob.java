package util;

import database.DatabaseManager;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import view.JobsWindowController;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class AddOrEditJob {

  private AnchorPane background;
  private JobsWindowController jobsWindowController;
  private DatabaseManager databaseManager;

  private AnchorPane addOrEditPane;
  private Rectangle backRec;
  private TextField jobTitleTextField;
  private ComboBox companyChooser;
  private TextField companyTextField;
  private ComboBox urgencyChooser;
  private TextField salaryTextField;
  private TextField linkTextField;
  private ComboBox statusChooser;
  private Label saveLabel;

  private boolean editing = false;
  private int taskID;


  public AddOrEditJob(AnchorPane background, JobsWindowController windowController, DatabaseManager databaseManager) {

    this.background = background;
    this.jobsWindowController = windowController;
    this.databaseManager = databaseManager;
    createWindow();
    createHandlers();

  }


  private void createWindow() {

    addOrEditPane = new AnchorPane();
    addOrEditPane.setPrefSize(950, 800);
    addOrEditPane.getStyleClass().add("addOrEditBackground");

    backRec = new Rectangle(20, 20);
    backRec.setTranslateX(25);
    backRec.setTranslateY(25);
    backRec.getStyleClass().add("backRec");

    AnchorPane formPane = new AnchorPane();
    formPane.setPrefSize(600, 515);
    formPane.setTranslateX(175);
    formPane.setTranslateY(177);
    formPane.getStylesheets().clear();
    formPane.getStylesheets().add("/css/addOrEditStylesheet.css");
    formPane.getStyleClass().add("formPane");

    Label jobTitleLabel = new Label("Job Title");
    jobTitleLabel.setTranslateX(50);
    jobTitleLabel.setTranslateY(40);
    jobTitleLabel.getStyleClass().add("formLabel");

    jobTitleTextField = new TextField();
    jobTitleTextField.setPrefSize(500, 40);
    jobTitleTextField.setTranslateX(50);
    jobTitleTextField.setTranslateY(60);

    Label companyLabel = new Label("Company");
    companyLabel.setTranslateX(50);
    companyLabel.setTranslateY(130);
    companyLabel.getStyleClass().add("formLabel");

    companyChooser = new ComboBox();
    companyChooser.setPrefSize(500, 40);
    companyChooser.setTranslateX(50);
    companyChooser.setTranslateY(150);

    try {
      ArrayList<String> companies = databaseManager.readAllCompaniesEntries();

      Collections.sort(companies, new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {

          String[] a = o1.split("§");
          String[] b = o2.split("§");

          return a[1].compareTo(b[1]);

        }
      });

      for (String c : companies) {
        String[] s = c.split("§");
        companyChooser.getItems().add(s[1]);
      }

      companyChooser.getSelectionModel().select(companies.get(0).split("§")[1]);

    } catch (SQLException e) {
      e.printStackTrace();
    }

    /*companyTextField = new TextField();
    companyTextField.setPrefSize(500, 40);
    companyTextField.setTranslateX(50);
    companyTextField.setTranslateY(150);*/

    Label salaryLabel = new Label("Salary");
    salaryLabel.setTranslateX(50);
    salaryLabel.setTranslateY(220);
    salaryLabel.getStyleClass().add("formLabel");

    salaryTextField = new TextField();
    salaryTextField.setPrefSize(240, 40);
    salaryTextField.setTranslateX(50);
    salaryTextField.setTranslateY(240);
    salaryTextField.setText("0");

    Label urgencyLabel = new Label("Urgency");
    urgencyLabel.setTranslateX(300);
    urgencyLabel.setTranslateY(220);
    urgencyLabel.getStyleClass().add("formLabel");

    urgencyChooser = new ComboBox();
    urgencyChooser.setPrefSize(240, 40);
    urgencyChooser.setTranslateX(300);
    urgencyChooser.setTranslateY(240);
    urgencyChooser.getItems().addAll("LOW", "MEDIUM", "HIGH", "ASAP");
    urgencyChooser.getSelectionModel().select("ASAP");

    Label linkLabel = new Label("Link to job spec");
    linkLabel.setTranslateX(50);
    linkLabel.setTranslateY(310);
    linkLabel.getStyleClass().add("formLabel");

    linkTextField = new TextField();
    linkTextField.setPrefSize(500, 40);
    linkTextField.setTranslateX(50);
    linkTextField.setTranslateY(330);

    Label statusLabel = new Label("Status");
    statusLabel.setTranslateX(50);
    statusLabel.setTranslateY(400);
    statusLabel.getStyleClass().add("formLabel");

    statusChooser = new ComboBox();
    statusChooser.setPrefSize(240, 40);
    statusChooser.setTranslateX(50);
    statusChooser.setTranslateY(420);
    statusChooser.getItems().addAll("open", "on hold", "closed");
    statusChooser.getSelectionModel().select("open");

    saveLabel = new Label("Save");
    saveLabel.setPrefSize(240, 40);
    saveLabel.setTranslateX(300);
    saveLabel.setTranslateY(420);
    saveLabel.getStyleClass().add("saveLabel");

    formPane.getChildren().addAll(jobTitleLabel, jobTitleTextField, companyLabel, companyChooser, salaryLabel,
        salaryTextField, urgencyLabel, urgencyChooser, linkLabel, linkTextField, statusLabel, statusChooser, saveLabel);

    addOrEditPane.getChildren().addAll(backRec, formPane);

    background.getChildren().add(addOrEditPane);

  }


  public void edit(int taskID, String jobTitle, String company, String urgency, String salary, String link, String status) {

    editing = true;
    this.taskID = taskID;
    jobTitleTextField.setText(jobTitle);
    companyChooser.getSelectionModel().select(company);
    urgencyChooser.getSelectionModel().select(urgency);
    salaryTextField.setText(salary);
    linkTextField.setText(link);
    statusChooser.getSelectionModel().select(status);

  }


  private void createHandlers() {

    backRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        background.getChildren().remove(addOrEditPane);
      }
    });

    saveLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        if (!editing) {

          try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            databaseManager.createJobsEntry(
                jobTitleTextField.getText().replace("'", "''"),
                companyChooser.getValue().toString().replace("'", "''"),
                urgencyChooser.getValue().toString(),
                dtf.format(LocalDate.now()),
                salaryTextField.getText().replace("'", "''"),
                statusChooser.getValue().toString(),
                linkTextField.getText().replace("'", "''"),
                0, 0, 0);
            databaseManager.updateCompanyLastModifiedEntry(
                companyChooser.getValue().toString().replace("'", "''"),
                dtf.format(LocalDate.now()) );
            background.getChildren().remove(addOrEditPane);
            jobsWindowController.updateWindow(databaseManager.readAllJobsEntries());
          } catch (SQLException e) {
            e.printStackTrace();
          }

          System.out.println("SAVE TASK");
        } else {

          try {
            databaseManager.updateJobsEntryAfterEdit(
                taskID,
                jobTitleTextField.getText().replace("'", "''"),
                companyChooser.getValue().toString().replace("'", "''"),
                urgencyChooser.getValue().toString(),
                salaryTextField.getText().replace("'", "''"),
                statusChooser.getValue().toString(),
                linkTextField.getText().replace("'", "''"));
            background.getChildren().remove(addOrEditPane);
            jobsWindowController.updateWindow(databaseManager.readAllJobsEntries());
          } catch (SQLException e) {
            e.printStackTrace();
          }

        }
      }
    });

  }

}
