package util;

import javafx.event.EventHandler;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.Axis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;

import java.util.HashMap;
import java.util.Map;

public class CustomAreaChart extends AreaChart {

  private Axis xAxis;
  private Axis yAxis;
  private int x;
  private int y;
  private int width;
  private int height;
  private XYChart.Series data;
  private String label;
  private HashMap<String, XYChart.Series> dataMap;

  private boolean enlarged = false;
  private Rectangle backRec;
  private Label enlargedLabel;


  public CustomAreaChart(Axis axis, Axis axis1) {
    super(axis, axis1);
  }

  public CustomAreaChart(Axis xAxis, Axis yAxis, int x, int y, int width, int height, XYChart.Series data, Label enlargedLabel) {
    super(xAxis, yAxis);
    this.xAxis = xAxis;
    this.yAxis = yAxis;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.data = data;
    this.enlargedLabel = enlargedLabel;

    setAnimated(false);

    createCloseRec();
    createChart();
  }

  public CustomAreaChart(Axis xAxis, Axis yAxis, int x, int y, int width, int height, HashMap<String, XYChart.Series> dataMap, Label enlargedLabel) {
    super(xAxis, yAxis);
    this.xAxis = xAxis;
    this.yAxis = yAxis;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.dataMap = dataMap;
    this.enlargedLabel = enlargedLabel;

    setAnimated(false);

    createCloseRec();
    createChartFromList();
  }


  private void createCloseRec() {

    backRec = new Rectangle(20, 20);
    backRec.setTranslateX(880);
    backRec.setTranslateY(-75);
    backRec.getStyleClass().add("backRec");
    backRec.setVisible(false);

    backRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent mouseEvent) {
        shrink();
        mouseEvent.consume();
      }
    });

  }


  private void createChart() {

    setTranslateX(x);
    setTranslateY(y);
    setPrefSize(width, height);
    getData().add(data);
    getStyleClass().add("customAreaChart");

    getChildren().add(backRec);

  }


  private void createChartFromList() {

    setTranslateX(x);
    setTranslateY(y);
    setPrefSize(width, height);
    for (Map.Entry<String, Series> entry : dataMap.entrySet()) {
      XYChart.Series series = entry.getValue();
      series.setName(entry.getKey());
      getData().add(series);

    }
    getStyleClass().add("customAreaChartList");

    getChildren().add(backRec);

  }


  public void enlarge(int w, int h, int x1, int y1) {
    if (!enlarged) {
      setPrefSize(w, h);
      setTranslateX(x1);
      setTranslateY(y1);
      getStyleClass().add("enlarged");
      xAxis.setTickLabelsVisible(true);
      enlarged = true;
      backRec.setVisible(true);
      enlargedLabel.setVisible(true);
    }
  }


  private void shrink() {
    setPrefSize(width, height);
    setTranslateX(x);
    setTranslateY(y);
    getStyleClass().remove("enlarged");
    xAxis.setTickLabelsVisible(false);
    enlarged = false;
    backRec.setVisible(false);
    enlargedLabel.setVisible(false);
  }






}
