package util;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;

public class CompanyEntry {

  private AnchorPane background;
  private String companiesDetails;
  private ArrayList<String> jobsDetails;

  private AnchorPane addOrEditPane;
  private Rectangle backRec;

  private Label company;
  private Label lastModified;
  private ListView jobs;
  private Label cvsSent;
  private Label interviews;
  private Label placements;


  public CompanyEntry(AnchorPane background, String companiesDetails, ArrayList<String> jobsDetails) {

    this.background = background;
    this.companiesDetails = companiesDetails;
    this.jobsDetails = jobsDetails;
    createWindow();
    createHandlers();

  }


  private void createWindow() {

    // id, companyName, lastModified
    String[] c = companiesDetails.split("§");

    addOrEditPane = new AnchorPane();
    addOrEditPane.setPrefSize(950, 800);
    addOrEditPane.getStyleClass().add("addOrEditBackground");

    backRec = new Rectangle(20, 20);
    backRec.setTranslateX(25);
    backRec.setTranslateY(25);
    backRec.getStyleClass().add("backRec");

    AnchorPane formPane = new AnchorPane();
    formPane.setPrefSize(600, 530);
    formPane.setTranslateX(175);
    formPane.setTranslateY(170);
    formPane.getStylesheets().clear();
    formPane.getStylesheets().add("/css/addOrEditStylesheet.css");
    formPane.getStyleClass().add("formPane");

    Label companyLabel = new Label("Company");
    companyLabel.setTranslateX(50);
    companyLabel.setTranslateY(40);
    companyLabel.getStyleClass().add("formLabel");

    company = new Label(c[1]);
    company.setTranslateX(50);
    company.setTranslateY(65);
    company.setMaxWidth(250);
    company.getStyleClass().add("formText");

    Label lastModifiedLabel = new Label("Last modified");
    lastModifiedLabel.setTranslateX(340);
    lastModifiedLabel.setTranslateY(40);
    lastModifiedLabel.getStyleClass().add("formLabel");

    lastModified = new Label(c[2]);
    lastModified.setTranslateX(340);
    lastModified.setTranslateY(65);
    lastModified.getStyleClass().add("formText");

    Line divider1 = new Line(30, 105, 540, 105);
    divider1.getStyleClass().add("dividerLine");

    Label jobsLabel = new Label("Jobs");
    jobsLabel.setTranslateX(50);
    jobsLabel.setTranslateY(130);
    jobsLabel.getStyleClass().add("formLabel");

    jobs = new ListView();
    jobs.setPrefSize(500, 195);
    jobs.setTranslateX(50);
    jobs.setTranslateY(155);
    jobs.getStyleClass().add("listView");

    Line divider2 = new Line(30, 375, 540, 375);
    divider2.getStyleClass().add("dividerLine");

    int cs = 0;
    int in = 0;
    int pl = 0;

    for (String s : jobsDetails) {
      String[] sa = s.split("§");
      jobs.getItems().add(sa[1]);
      cs += Integer.parseInt(sa[8]);
      in += Integer.parseInt(sa[9]);
      pl += Integer.parseInt(sa[10]);
    }

    Label cvsSentLabel = new Label("CVs sent");
    cvsSentLabel.setTranslateX(50);
    cvsSentLabel.setTranslateY(400);
    cvsSentLabel.getStyleClass().add("formLabel");

    cvsSent = new Label(String.valueOf(cs));
    cvsSent.setTranslateX(170);
    cvsSent.setTranslateY(400);
    cvsSent.getStyleClass().add("formText");


    Label interviewsLabel = new Label("Interviews");
    interviewsLabel.setTranslateX(50);
    interviewsLabel.setTranslateY(430);
    interviewsLabel.getStyleClass().add("formLabel");

    interviews = new Label(String.valueOf(in));
    interviews.setTranslateX(170);
    interviews.setTranslateY(430);
    interviews.getStyleClass().add("formText");


    Label placementsLabel = new Label("Placements");
    placementsLabel.setTranslateX(50);
    placementsLabel.setTranslateY(460);
    placementsLabel.getStyleClass().add("formLabel");

    placements = new Label(String.valueOf(pl));
    placements.setTranslateX(170);
    placements.setTranslateY(460);
    placements.getStyleClass().add("formText");



    formPane.getChildren().addAll(companyLabel, company, lastModifiedLabel, lastModified, divider1,
        jobsLabel, jobs, divider2, cvsSentLabel, cvsSent, interviewsLabel, interviews, placementsLabel, placements);

    addOrEditPane.getChildren().addAll(backRec, formPane);

    background.getChildren().add(addOrEditPane);

  }


  private void createHandlers() {

    backRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        background.getChildren().remove(addOrEditPane);
      }
    });

  }

}
