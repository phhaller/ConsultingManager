package util;

import database.DatabaseManager;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import view.CalendarWindowController;
import view.TasksWindowController;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class AddOrEditTask {

  private AnchorPane background;
  private TasksWindowController tasksWindowController;
  private CalendarWindowController calendarWindowController;
  private DatabaseManager databaseManager;

  private AnchorPane addOrEditPane;
  private Rectangle backRec;
  private ComboBox urgencyChooser;
  private DatePicker dueDatePicker;
  private TextArea taskTextArea;
  private ComboBox typeChooser;
  private Label saveLabel;

  private boolean editing = false;
  private int taskID;


  public AddOrEditTask(AnchorPane background, TasksWindowController windowController, DatabaseManager databaseManager) {

    this.background = background;
    this.tasksWindowController = windowController;
    this.databaseManager = databaseManager;
    createWindow();
    createHandlers();

  }

  public AddOrEditTask(AnchorPane background, CalendarWindowController windowController, DatabaseManager databaseManager) {

    this.background = background;
    this.calendarWindowController = windowController;
    this.databaseManager = databaseManager;
    createWindow();
    createHandlers();

  }


  private void createWindow() {

    addOrEditPane = new AnchorPane();
    addOrEditPane.setPrefSize(950, 800);
    addOrEditPane.getStyleClass().add("addOrEditBackground");

    backRec = new Rectangle(20, 20);
    backRec.setTranslateX(25);
    backRec.setTranslateY(25);
    backRec.getStyleClass().add("backRec");

    AnchorPane formPane = new AnchorPane();
    formPane.setPrefSize(600, 370);
    formPane.setTranslateX(175);
    formPane.setTranslateY(235);
    formPane.getStylesheets().clear();
    formPane.getStylesheets().add("/css/addOrEditStylesheet.css");
    formPane.getStyleClass().add("formPane");

    Label urgencyLabel = new Label("Urgency");
    urgencyLabel.setTranslateX(50);
    urgencyLabel.setTranslateY(40);
    urgencyLabel.getStyleClass().add("formLabel");

    urgencyChooser = new ComboBox();
    urgencyChooser.setPrefSize(240, 40);
    urgencyChooser.setTranslateX(50);
    urgencyChooser.setTranslateY(60);
    urgencyChooser.getItems().addAll("LOW", "MEDIUM", "HIGH");
    urgencyChooser.getSelectionModel().select("LOW");

    Label dueDateLabel = new Label("Due date");
    dueDateLabel.setTranslateX(300);
    dueDateLabel.setTranslateY(40);
    dueDateLabel.getStyleClass().add("formLabel");

    dueDatePicker = new DatePicker();
    dueDatePicker.setPrefSize(240, 40);
    dueDatePicker.setTranslateX(300);
    dueDatePicker.setTranslateY(60);
    dueDatePicker.setValue(LocalDate.now());
    dueDatePicker.getEditor().setDisable(true);
    dueDatePicker.getEditor().setOpacity(1);

    Label taskLabel = new Label("Task");
    taskLabel.setTranslateX(50);
    taskLabel.setTranslateY(130);
    taskLabel.getStyleClass().add("formLabel");

    taskTextArea = new TextArea();
    taskTextArea.setPrefSize(500, 80);
    taskTextArea.setTranslateX(50);
    taskTextArea.setTranslateY(150);

    Label typeLabel = new Label("Type");
    typeLabel.setTranslateX(50);
    typeLabel.setTranslateY(260);
    typeLabel.getStyleClass().add("formLabel");

    typeChooser = new ComboBox();
    typeChooser.setPrefSize(240, 40);
    typeChooser.setTranslateX(50);
    typeChooser.setTranslateY(280);
    typeChooser.getItems().addAll("General", "LinkedIn", "Call", "Meeting", "Interview", "Placement");
    typeChooser.getSelectionModel().select("General");

    saveLabel = new Label("Save");
    saveLabel.setPrefSize(240, 40);
    saveLabel.setTranslateX(300);
    saveLabel.setTranslateY(280);
    saveLabel.getStyleClass().add("saveLabel");

    formPane.getChildren().addAll(urgencyLabel, urgencyChooser, dueDateLabel, dueDatePicker, taskLabel, taskTextArea,
        typeLabel, typeChooser, saveLabel);

    addOrEditPane.getChildren().addAll(backRec, formPane);

    background.getChildren().add(addOrEditPane);

  }


  public void edit(int taskID, String urgency, String date, String task, String type) {

    editing = true;
    this.taskID = taskID;
    urgencyChooser.getSelectionModel().select(urgency);
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    dueDatePicker.setValue(LocalDate.parse(date, dtf));
    taskTextArea.setText(task);
    typeChooser.getSelectionModel().select(type);

  }


  private void createHandlers() {

    backRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        background.getChildren().remove(addOrEditPane);

      }
    });

    taskTextArea.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
      @Override
      public void handle(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
          event.consume();
        }
      }
    });

    saveLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        if (!editing) {

          try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            databaseManager.createTaskEntry(
                urgencyChooser.getValue().toString(),
                dtf.format(dueDatePicker.getValue()),
                taskTextArea.getText().replace("'", "''"),
                typeChooser.getValue().toString());
            background.getChildren().remove(addOrEditPane);
            if (tasksWindowController != null) {
              updateTaskWindow();
            }
            if (calendarWindowController != null) {
              calendarWindowController.update();
            }
          } catch (SQLException e) {
            e.printStackTrace();
          }

        } else {

          try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            databaseManager.updateTaskEntry(
                taskID,
                urgencyChooser.getValue().toString(),
                dtf.format(dueDatePicker.getValue()),
                taskTextArea.getText().replace("'", "''"),
                typeChooser.getValue().toString());
            background.getChildren().remove(addOrEditPane);
            if (tasksWindowController != null) {
              updateTaskWindow();
            }
            if (calendarWindowController != null) {
              calendarWindowController.update();
            }
          } catch (SQLException e) {
            e.printStackTrace();
          }

        }
      }
    });

  }


  private void updateTaskWindow() {
    try {
      tasksWindowController.updateWindow(databaseManager.readAllTaskEntries());
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

}
