package util;

import database.DatabaseManager;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;

public class JobEntry {

  private AnchorPane background;
  private String jobDetails;

  private AnchorPane addOrEditPane;
  private Rectangle backRec;

  private Label jobTitle;
  private Label company;
  private Label salary;
  private Label status;
  private Label urgency;
  private Label dateAdded;
  private Label link;
  private Label cvsSent;
  private Label interviews;
  private Rectangle placementRec;


  public JobEntry(AnchorPane background, String jobDetails) {

    this.background = background;
    this.jobDetails = jobDetails;
    createWindow();
    createHandlers();

  }


  private void createWindow() {

    // id, jobTitle, company, urgency, dateAdded, salary, status, link, cvsSent, interviews, placement
    String[] s = jobDetails.split("§");

    addOrEditPane = new AnchorPane();
    addOrEditPane.setPrefSize(950, 800);
    addOrEditPane.getStyleClass().add("addOrEditBackground");

    backRec = new Rectangle(20, 20);
    backRec.setTranslateX(25);
    backRec.setTranslateY(25);
    backRec.getStyleClass().add("backRec");

    AnchorPane formPane = new AnchorPane();
    formPane.setPrefSize(600, 530);
    formPane.setTranslateX(175);
    formPane.setTranslateY(170);
    formPane.getStylesheets().clear();
    formPane.getStylesheets().add("/css/addOrEditStylesheet.css");
    formPane.getStyleClass().add("formPane");

    Label jobTitleLabel = new Label("Job Title");
    jobTitleLabel.setTranslateX(50);
    jobTitleLabel.setTranslateY(40);
    jobTitleLabel.getStyleClass().add("formLabel");

    jobTitle = new Label(s[1]);
    jobTitle.setTranslateX(50);
    jobTitle.setTranslateY(65);
    jobTitle.setMaxWidth(250);
    jobTitle.getStyleClass().add("formText");

    Label companyLabel = new Label("Company");
    companyLabel.setTranslateX(340);
    companyLabel.setTranslateY(40);
    companyLabel.getStyleClass().add("formLabel");

    company = new Label(s[2]);
    company.setTranslateX(340);
    company.setTranslateY(65);
    company.setMaxWidth(250);
    company.getStyleClass().add("formText");

    Line divider1 = new Line(30, 105, 540, 105);
    divider1.getStyleClass().add("dividerLine");


    Label salaryLabel = new Label("Salary");
    salaryLabel.setTranslateX(50);
    salaryLabel.setTranslateY(130);
    salaryLabel.getStyleClass().add("formLabel");

    salary = new Label(s[5]);
    salary.setTranslateX(50);
    salary.setTranslateY(155);
    salary.getStyleClass().add("formText");

    Label statusLabel = new Label("Status");
    statusLabel.setTranslateX(340);
    statusLabel.setTranslateY(130);
    statusLabel.getStyleClass().add("formLabel");

    status = new Label(s[6]);
    status.setTranslateX(340);
    status.setTranslateY(155);
    status.getStyleClass().add("formText");

    Line divider2 = new Line(30, 195, 540, 195);
    divider2.getStyleClass().add("dividerLine");


    Label urgencyLabel = new Label("Urgency");
    urgencyLabel.setTranslateX(50);
    urgencyLabel.setTranslateY(220);
    urgencyLabel.getStyleClass().add("formLabel");

    urgency = new Label(s[3]);
    urgency.setTranslateX(50);
    urgency.setTranslateY(245);
    urgency.getStyleClass().add("formText");

    Label dateAddedLabel = new Label("Date added");
    dateAddedLabel.setTranslateX(340);
    dateAddedLabel.setTranslateY(220);
    dateAddedLabel.getStyleClass().add("formLabel");

    dateAdded = new Label(s[4]);
    dateAdded.setTranslateX(340);
    dateAdded.setTranslateY(245);
    dateAdded.getStyleClass().add("formText");

    Line divider3 = new Line(30, 285, 540, 285);
    divider3.getStyleClass().add("dividerLine");


    Label linkLabel = new Label("Website");
    linkLabel.setTranslateX(50);
    linkLabel.setTranslateY(310);
    linkLabel.getStyleClass().add("formLabel");

    link = new Label(s[7]);
    link.setTranslateX(50);
    link.setTranslateY(335);
    link.setMaxWidth(500);
    link.getStyleClass().add("formText");

    Line divider4 = new Line(30, 375, 540, 375);
    divider4.getStyleClass().add("dividerLine");


    Label cvsSentLabel = new Label("CVs sent");
    cvsSentLabel.setTranslateX(50);
    cvsSentLabel.setTranslateY(400);
    cvsSentLabel.getStyleClass().add("formLabel");

    cvsSent = new Label(s[8]);
    cvsSent.setTranslateX(170);
    cvsSent.setTranslateY(400);
    cvsSent.getStyleClass().add("formText");


    Label interviewsLabel = new Label("Interviews");
    interviewsLabel.setTranslateX(50);
    interviewsLabel.setTranslateY(430);
    interviewsLabel.getStyleClass().add("formLabel");

    interviews = new Label(s[9]);
    interviews.setTranslateX(170);
    interviews.setTranslateY(430);
    interviews.getStyleClass().add("formText");


    Label placementLabel = new Label("Placement");
    placementLabel.setTranslateX(50);
    placementLabel.setTranslateY(460);
    placementLabel.getStyleClass().add("formLabel");

    placementRec = new Rectangle(26, 26);
    placementRec.setTranslateX(170);
    placementRec.setTranslateY(457);

    if (Integer.parseInt(s[10]) < 1) {
      placementRec.getStyleClass().add("noPlacementRec");
    } else {
      placementRec.getStyleClass().add("placementRec");
    }


    formPane.getChildren().addAll(jobTitleLabel, jobTitle, companyLabel, company, divider1,
                                  salaryLabel, salary, statusLabel, status, divider2,
                                  urgencyLabel, urgency, dateAddedLabel, dateAdded, divider3,
                                  linkLabel, link, divider4,
                                  cvsSentLabel, cvsSent, interviewsLabel, interviews, placementLabel, placementRec);

    addOrEditPane.getChildren().addAll(backRec, formPane);

    background.getChildren().add(addOrEditPane);

  }


  private void createHandlers() {

    backRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        background.getChildren().remove(addOrEditPane);
      }
    });

  }


}
