package util;

import database.DatabaseManager;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import view.CandidateAllWindowController;

import java.sql.SQLException;
import java.time.format.DateTimeFormatter;

public class AddOrEditCandidatesAll {

  private AnchorPane background;
  private CandidateAllWindowController candidateAllWindowController;
  private DatabaseManager databaseManager;

  private AnchorPane addOrEditPane;
  private Rectangle backRec;
  private TextField firstNameTextField;
  private TextField lastNameTextField;
  private TextField jobTextField;
  private TextField salaryTextField;
  private Label saveLabel;

  private boolean editing = false;
  private int taskID;

  public AddOrEditCandidatesAll(AnchorPane background, CandidateAllWindowController candidateAllWindowController, DatabaseManager databaseManager) {

    this.background = background;
    this.candidateAllWindowController = candidateAllWindowController;
    this.databaseManager = databaseManager;
    createWindow();
    createHandlers();

  }


  private void createWindow() {

    addOrEditPane = new AnchorPane();
    addOrEditPane.setPrefSize(950, 800);
    addOrEditPane.getStyleClass().add("addOrEditBackground");

    backRec = new Rectangle(20, 20);
    backRec.setTranslateX(25);
    backRec.setTranslateY(25);
    backRec.getStyleClass().add("backRec");

    AnchorPane formPane = new AnchorPane();
    formPane.setPrefSize(600, 335);
    formPane.setTranslateX(198);
    formPane.setTranslateY(235);
    formPane.getStylesheets().clear();
    formPane.getStylesheets().add("/css/addOrEditStylesheet.css");
    formPane.getStyleClass().add("formPane");

    Label firstNameLabel = new Label("First name");
    firstNameLabel.setTranslateX(50);
    firstNameLabel.setTranslateY(40);
    firstNameLabel.getStyleClass().add("formLabel");

    firstNameTextField = new TextField();
    firstNameTextField.setPrefSize(240, 40);
    firstNameTextField.setTranslateX(50);
    firstNameTextField.setTranslateY(60);

    Label lastNameLabel = new Label("Last name");
    lastNameLabel.setTranslateX(300);
    lastNameLabel.setTranslateY(40);
    lastNameLabel.getStyleClass().add("formLabel");

    lastNameTextField = new TextField();
    lastNameTextField.setPrefSize(240, 40);
    lastNameTextField.setTranslateX(300);
    lastNameTextField.setTranslateY(60);

    Label jobLabel = new Label("Job");
    jobLabel.setTranslateX(50);
    jobLabel.setTranslateY(130);
    jobLabel.getStyleClass().add("formLabel");

    jobTextField = new TextField();
    jobTextField.setPrefSize(500, 40);
    jobTextField.setTranslateX(50);
    jobTextField.setTranslateY(150);

    Label salaryLabel = new Label("Salary");
    salaryLabel.setTranslateX(50);
    salaryLabel.setTranslateY(220);
    salaryLabel.getStyleClass().add("formLabel");

    salaryTextField = new TextField();
    salaryTextField.setPrefSize(240, 40);
    salaryTextField.setTranslateX(50);
    salaryTextField.setTranslateY(240);

    saveLabel = new Label("Save");
    saveLabel.setPrefSize(240, 40);
    saveLabel.setTranslateX(300);
    saveLabel.setTranslateY(240);
    saveLabel.getStyleClass().add("saveLabel");

    formPane.getChildren().addAll(firstNameLabel, firstNameTextField, lastNameLabel, lastNameTextField, jobLabel, jobTextField,
        salaryLabel, salaryTextField, saveLabel);

    addOrEditPane.getChildren().addAll(backRec, formPane);

    background.getChildren().add(addOrEditPane);

  }


  public void edit(int taskID, String firstName, String lastName, String job, String salary) {

    editing = true;
    this.taskID = taskID;
    firstNameTextField.setText(firstName);
    lastNameTextField.setText(lastName);
    jobTextField.setText(job);
    salaryTextField.setText(salary);

  }


  private void createHandlers() {

    backRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        background.getChildren().remove(addOrEditPane);

      }
    });


    saveLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        if (!editing) {

          try {
            databaseManager.createCandidatesAllEntry(
                firstNameTextField.getText().replace("'", "''"),
                lastNameTextField.getText().replace("'", "''"),
                jobTextField.getText().replace("'", "''"),
                salaryTextField.getText().replace("'", "''"),
                0, 0, 0, 0, 0);
            background.getChildren().remove(addOrEditPane);
            candidateAllWindowController.updateWindow(databaseManager.readAllCandidatesAllEntries());
          } catch (SQLException e) {
            e.printStackTrace();
          }

        } else {

          try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            databaseManager.updateCandidatesAllEntryAfterEdit(
                taskID,
                firstNameTextField.getText().replace("'", "''"),
                lastNameTextField.getText().replace("'", "''"),
                jobTextField.getText().replace("'", "''"),
                salaryTextField.getText().replace("'", "''"));
            background.getChildren().remove(addOrEditPane);
            candidateAllWindowController.updateWindow(databaseManager.readAllCandidatesAllEntries());
          } catch (SQLException e) {
            e.printStackTrace();
          }

        }

      }
    });

  }

}
