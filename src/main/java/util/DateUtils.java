package util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.Locale;

public class DateUtils {

  private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");

  public static LocalDate getDateFromString(String date) {
    return LocalDate.parse(date, dtf);
  }

  public static String formatDate(LocalDate date) {
    return dtf.format(date);
  }

  public static String getTodayDate() {
    return dtf.format(LocalDate.now());
  }

  public static String getCurrentYear() {
    return String.valueOf(LocalDate.now().getYear());
  }

  public static String getWeekFromDate(LocalDate date) {
    WeekFields weekFields = WeekFields.of(Locale.getDefault());
    return String.valueOf(date.get(weekFields.weekOfWeekBasedYear()));
  }

  public static String getMonthNameFromDate(LocalDate date) {
    return date.getMonth().toString();
  }

  public static String getMonthNumberFromDate(LocalDate date) {
    return String.valueOf(date.getMonthValue());
  }

  public static String getYearFromDate(LocalDate date) {
    return String.valueOf(date.getYear());
  }


}
