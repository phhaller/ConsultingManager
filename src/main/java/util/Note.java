package util;

import database.DatabaseManager;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import view.CandidateNotesWindowController;
import view.JobsNotesWindowController;

import java.sql.SQLException;

public class Note extends AnchorPane {

  private int x;
  private int y;
  private int id;
  private String subject;
  private String subjectTwo;
  private String note;
  private String date;
  private Rectangle editRec;
  private Rectangle deleteRec;
  private AnchorPane rootPane;
  private JobsNotesWindowController jobsNotesWindowController;
  private CandidateNotesWindowController candidateNotesWindowController;
  private DatabaseManager databaseManager;

  public Note(int x, int y, int id, String subject, String subjectTwo, String note, String date, AnchorPane rootPane,
              JobsNotesWindowController jobsNotesWindowController, DatabaseManager databaseManager) {
    this.x = x;
    this.y = y;
    this.id = id;
    this.subject = subject;
    this.subjectTwo = subjectTwo;
    this.note = note;
    this.date = date;
    this.rootPane = rootPane;
    this.jobsNotesWindowController = jobsNotesWindowController;
    this.databaseManager = databaseManager;

    createNote();
    createHandlers();
  }

  public Note(int x, int y, int id, String subject, String subjectTwo, String note, String date, AnchorPane rootPane,
              CandidateNotesWindowController candidateNotesWindowController, DatabaseManager databaseManager) {
    this.x = x;
    this.y = y;
    this.id = id;
    this.subject = subject;
    this.subjectTwo = subjectTwo;
    this.note = note;
    this.date = date;
    this.rootPane = rootPane;
    this.candidateNotesWindowController = candidateNotesWindowController;
    this.databaseManager = databaseManager;

    createNote();
    createHandlers();
  }


  private void createNote() {

    setPrefSize(290, 290);
    setTranslateX(x);
    setTranslateY(y);
    getStyleClass().add("notePane");

    Label subjectLabel = null;
    Label subjectTwoLabel = null;
    Line divider1 = null;
    TextArea noteArea = new TextArea(note);
    noteArea.setEditable(false);

    if (subject.isEmpty() && subjectTwo.isEmpty()) {
      noteArea.setPrefSize(270, 220);
      noteArea.setTranslateX(10);
      noteArea.setTranslateY(20);
    } else if (subjectTwo.isEmpty()) {
      subjectLabel = new Label(subject);
      subjectLabel.setTranslateX(20);
      subjectLabel.setTranslateY(20);
      subjectLabel.setMaxSize(250, 20);
      subjectLabel.getStyleClass().add("subjectLabel");

      divider1 = new Line(20, 50, 250, 50);
      divider1.getStyleClass().add("dividerLine");

      noteArea.setPrefSize(270, 185);
      noteArea.setTranslateX(10);
      noteArea.setTranslateY(55);
    } else {
      subjectLabel = new Label(subject);
      subjectLabel.setTranslateX(20);
      subjectLabel.setTranslateY(20);
      subjectLabel.setMaxSize(250, 20);
      subjectLabel.getStyleClass().add("subjectLabel");

      subjectTwoLabel = new Label(subjectTwo);
      subjectTwoLabel.setTranslateX(20);
      subjectTwoLabel.setTranslateY(45);
      subjectTwoLabel.setMaxSize(250, 20);
      subjectTwoLabel.getStyleClass().add("subjectTwoLabel");

      divider1 = new Line(20, 75, 250, 75);
      divider1.getStyleClass().add("dividerLine");

      noteArea.setPrefSize(270, 160);
      noteArea.setTranslateX(10);
      noteArea.setTranslateY(80);
    }

    Label dateLabel = new Label(date);
    dateLabel.setTranslateX(20);
    dateLabel.setTranslateY(255);
    dateLabel.getStyleClass().add("");

    editRec = new Rectangle(20, 20);
    editRec.setTranslateX(220);
    editRec.setTranslateY(255);
    editRec.getStyleClass().add("editRec");
    editRec.setVisible(false);

    deleteRec = new Rectangle(20, 20);
    deleteRec.setTranslateX(250);
    deleteRec.setTranslateY(255);
    deleteRec.getStyleClass().add("deleteRec");
    deleteRec.setVisible(false);

    if (subject.isEmpty() && subjectTwo.isEmpty()) {
      getChildren().addAll(noteArea, dateLabel, editRec, deleteRec);
    } else if (subjectTwo.isEmpty()) {
      getChildren().addAll(subjectLabel, divider1, noteArea, dateLabel, editRec, deleteRec);
    } else {
      getChildren().addAll(subjectLabel, subjectTwoLabel, divider1, noteArea, dateLabel, editRec, deleteRec);
    }

  }


  private void createHandlers() {

    addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        editRec.setVisible(true);
        deleteRec.setVisible(true);
      }
    });

    addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        editRec.setVisible(false);
        deleteRec.setVisible(false);
      }
    });

    editRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        if (jobsNotesWindowController == null) {
          AddOrEditCandidateNote addOrEditCandidateNote = new AddOrEditCandidateNote(rootPane, candidateNotesWindowController, databaseManager);
          addOrEditCandidateNote.edit(id, subject, subjectTwo, note);
        } else {
          AddOrEditJobNote addOrEditJobNote = new AddOrEditJobNote(rootPane, jobsNotesWindowController, databaseManager);
          addOrEditJobNote.edit(id, subject, subjectTwo, note);
        }
      }
    });

    deleteRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        try {
          if (jobsNotesWindowController == null) {
            databaseManager.deleteCandidatesNotesEntry(id);
            candidateNotesWindowController.updateWindow(databaseManager.readAllCandidatesNotesEntries());
          } else {
            databaseManager.deleteJobsNotesEntry(id);
            jobsNotesWindowController.updateWindow(databaseManager.readAllJobsNotesEntries());
          }
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    });

  }


  public int getTaskId() {
    return id;
  }


}
