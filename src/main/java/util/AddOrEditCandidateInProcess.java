package util;

import database.DatabaseManager;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import view.CandidateInProcessWindowController;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class AddOrEditCandidateInProcess {

  private AnchorPane background;
  private CandidateInProcessWindowController candidateInProcessWindowController;
  private DatabaseManager databaseManager;

  private AnchorPane addOrEditPane;
  private Rectangle backRec;
  private ComboBox candidateChooser;
  private ComboBox companyChooser;
  private ComboBox jobChooser;
  private ComboBox statusChooser;
  private Label saveLabel;
  private ArrayList<Integer> jobIds = new ArrayList<>();
  private String currentStatus;
  private ArrayList<Integer> candidateIds = new ArrayList<>();

  private boolean editing = false;
  private int taskID;

  public AddOrEditCandidateInProcess(AnchorPane background, CandidateInProcessWindowController candidateInProcessWindowController, DatabaseManager databaseManager) {

    this.background = background;
    this.candidateInProcessWindowController = candidateInProcessWindowController;
    this.databaseManager = databaseManager;
    createWindow();
    createHandlers();

  }


  private void createWindow() {

    addOrEditPane = new AnchorPane();
    addOrEditPane.setPrefSize(950, 800);
    addOrEditPane.getStyleClass().add("addOrEditBackground");

    backRec = new Rectangle(20, 20);
    backRec.setTranslateX(25);
    backRec.setTranslateY(25);
    backRec.getStyleClass().add("backRec");

    AnchorPane formPane = new AnchorPane();
    formPane.setPrefSize(600, 425);
    formPane.setTranslateX(198);
    formPane.setTranslateY(222.5);
    formPane.getStylesheets().clear();
    formPane.getStylesheets().add("/css/addOrEditStylesheet.css");
    formPane.getStyleClass().add("formPane");

    Label candidateLabel = new Label("Candidate");
    candidateLabel.setTranslateX(50);
    candidateLabel.setTranslateY(40);
    candidateLabel.getStyleClass().add("formLabel");

    candidateChooser = new ComboBox();
    candidateChooser.setPrefSize(500, 40);
    candidateChooser.setTranslateX(50);
    candidateChooser.setTranslateY(60);

    try {
      ArrayList<String> candidates = databaseManager.readAllCandidatesAllEntries();

      Collections.sort(candidates, new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {

          String[] a = o1.split("§");
          String[] b = o2.split("§");

          return a[2].compareTo(b[2]);

        }
      });

      candidateChooser.getItems().add("");

      for (String c : candidates) {
        String[] s = c.split("§");
        candidateChooser.getItems().add(s[1] + " " + s[2]);
        candidateIds.add(Integer.parseInt(s[0]));
      }

      candidateChooser.getSelectionModel().select("");

    } catch (SQLException e) {
      e.printStackTrace();
    }

    Label companyLabel = new Label("Company");
    companyLabel.setTranslateX(50);
    companyLabel.setTranslateY(130);
    companyLabel.getStyleClass().add("formLabel");

    companyChooser = new ComboBox();
    companyChooser.setPrefSize(500, 40);
    companyChooser.setTranslateX(50);
    companyChooser.setTranslateY(150);

    try {
      ArrayList<String> companies = databaseManager.readAllCompaniesEntries();

      Collections.sort(companies, new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {

          String[] a = o1.split("§");
          String[] b = o2.split("§");

          return a[1].compareTo(b[1]);

        }
      });

      companyChooser.getItems().add("");

      for (String c : companies) {
        String[] s = c.split("§");
        companyChooser.getItems().add(s[1]);
      }

      companyChooser.getSelectionModel().select("");

    } catch (SQLException e) {
      e.printStackTrace();
    }

    Label jobTitleLabel = new Label("Job Title");
    jobTitleLabel.setTranslateX(50);
    jobTitleLabel.setTranslateY(220);
    jobTitleLabel.getStyleClass().add("formLabel");

    jobChooser = new ComboBox();
    jobChooser.setPrefSize(500, 40);
    jobChooser.setTranslateX(50);
    jobChooser.setTranslateY(240);

    try {
      addJobsToChooser(databaseManager.readAllJobsEntries());
    } catch (SQLException e) {
      e.printStackTrace();
    }

    Label statusLabel = new Label("Status");
    statusLabel.setTranslateX(50);
    statusLabel.setTranslateY(310);
    statusLabel.getStyleClass().add("formLabel");

    statusChooser = new ComboBox();
    statusChooser.setPrefSize(240, 40);
    statusChooser.setTranslateX(50);
    statusChooser.setTranslateY(330);
    statusChooser.getItems().addAll("CV sent", "Interview", "Placement", "Rejected");
    statusChooser.getSelectionModel().select("CV sent");

    saveLabel = new Label("Save");
    saveLabel.setPrefSize(240, 40);
    saveLabel.setTranslateX(300);
    saveLabel.setTranslateY(330);
    saveLabel.getStyleClass().add("saveLabel");

    formPane.getChildren().addAll(candidateLabel, candidateChooser, jobTitleLabel, jobChooser, companyLabel, companyChooser,
        statusLabel, statusChooser, saveLabel);

    addOrEditPane.getChildren().addAll(backRec, formPane);

    background.getChildren().add(addOrEditPane);

  }


  public void edit(int taskID, String name, String company, String job, String status) {

    editing = true;
    this.taskID = taskID;
    candidateChooser.getSelectionModel().select(name);
    companyChooser.getSelectionModel().select(company);
    jobChooser.getSelectionModel().select(job);
    statusChooser.getSelectionModel().select(status);
    currentStatus = status;

  }


  private void addJobsToChooser(ArrayList<String> list) {

    jobChooser.getItems().clear();
    jobIds.clear();

    Collections.sort(list, new Comparator<String>() {
      @Override
      public int compare(String o1, String o2) {

        String[] a = o1.split("§");
        String[] b = o2.split("§");

        return a[1].compareTo(b[1]);

      }
    });

    jobChooser.getItems().add("");

    for (String c : list) {
      String[] s = c.split("§");
      if (s[6].equals("open")) {
        jobChooser.getItems().add(s[1]);
        jobIds.add(Integer.parseInt(s[0]));
      }
    }

    jobChooser.getSelectionModel().select("");

  }


  private void createHandlers() {

    backRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        background.getChildren().remove(addOrEditPane);

      }
    });


    companyChooser.valueProperty().addListener(new ChangeListener() {
      @Override
      public void changed(ObservableValue observable, Object oldValue, Object newValue) {

        if (newValue.equals("")) {
          try {
            addJobsToChooser(databaseManager.readAllJobsEntries());
          } catch (SQLException e) {
            e.printStackTrace();
          }
        } else {
          try {
            addJobsToChooser(databaseManager.readJobsEntriesByCompany(newValue.toString()));
          } catch (SQLException e) {
            e.printStackTrace();
          }
        }

      }
    });



    /* THIS PART NEEDS SOME UPDATING AND CHECKING */

    saveLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");

        if (!editing) {

          try {
            databaseManager.createCandidatesInProcessEntry(
                candidateChooser.getValue().toString().replace("'", "''"),
                jobChooser.getValue().toString().replace("'", "''"),
                companyChooser.getValue().toString().replace("'", "''"),
                statusChooser.getValue().toString().replace("'", "''"),
                dtf.format(LocalDate.now()), dtf.format(LocalDate.now()));
            background.getChildren().remove(addOrEditPane);
            candidateInProcessWindowController.updateWindow(databaseManager.readAllCandidatesInProcessEntries());
            int jobIndex = jobChooser.getSelectionModel().getSelectedIndex();
            int jobID = jobIds.get(jobIndex - 1);
            int jobCV = Integer.parseInt(databaseManager.readJobByID(jobID).split("§")[8]);
            databaseManager.updateJobsCvSentEntry(jobIds.get(jobIndex-1), jobCV + 1);
            int candidateIndex = candidateChooser.getSelectionModel().getSelectedIndex();
            int candidateID = candidateIds.get(candidateIndex - 1);
            String[] tmp = databaseManager.readCandidatesAllByID(candidateID).split("§");
            int processes = Integer.parseInt(tmp[5]);
            int candidateCV = Integer.parseInt(tmp[6]);
            databaseManager.updateCandidatesAllProcesses(candidateID, processes + 1);
            databaseManager.updateCandidatesAllCvsSent(candidateID, candidateCV + 1);
            String[] candidatesStatistics = databaseManager.readCandidatesStatisticsEntryByDate(DateUtils.getTodayDate()).split("§");
            int candidateStatisticsCV = Integer.parseInt(candidatesStatistics[2]);
            int candidateStatisticsInt = Integer.parseInt(candidatesStatistics[3]);
            int candidateStatisticsPlace = Integer.parseInt(candidatesStatistics[4]);
            databaseManager.updateCandidatesStatisticsEntryByDate(DateUtils.getTodayDate(), candidateStatisticsCV + 1, candidateStatisticsInt, candidateStatisticsPlace);
          } catch (SQLException e) {
            e.printStackTrace();
          }

        } else {

          try {

            databaseManager.updateCandidatesInProcessEntryAfterEdit(
                taskID,
                candidateChooser.getValue().toString().replace("'", "''"),
                jobChooser.getValue().toString().replace("'", "''"),
                companyChooser.getValue().toString().replace("'", "''"),
                statusChooser.getValue().toString().replace("'", "''"),
                dtf.format(LocalDate.now()));
            background.getChildren().remove(addOrEditPane);
            candidateInProcessWindowController.updateWindow(databaseManager.readAllCandidatesInProcessEntries());


            int index = jobChooser.getSelectionModel().getSelectedIndex();
            int jobID = jobIds.get(index - 1);
            String[] tmp1 = databaseManager.readJobByID(jobID).split("§");
            int jobInt = Integer.parseInt(tmp1[9]);
            int jobPlace = Integer.parseInt(tmp1[10]);


            int candidateIndex = candidateChooser.getSelectionModel().getSelectedIndex();
            int candidateID = candidateIds.get(candidateIndex - 1);
            String[] tmp2 = databaseManager.readCandidatesAllByID(candidateID).split("§");
            int processes = Integer.parseInt(tmp2[5]);
            int candidateInt = Integer.parseInt(tmp2[7]);
            int candidatePlace = Integer.parseInt(tmp2[8]);
            int candidateRejec = Integer.parseInt(tmp2[9]);

            String[] candidatesStatistics = databaseManager.readCandidatesStatisticsEntryByDate(DateUtils.getTodayDate()).split("§");
            int candidateStatisticsCV = Integer.parseInt(candidatesStatistics[2]);
            int candidateStatisticsInt = Integer.parseInt(candidatesStatistics[3]);
            int candidateStatisticsPlace = Integer.parseInt(candidatesStatistics[4]);


            if (currentStatus.equals("CV sent")) {
              if (statusChooser.getValue().toString().replace("'", "''").equals("Interview")) {
                databaseManager.updateJobsInterviewsEntry(jobID, jobInt + 1);
                databaseManager.updateCandidatesAllInterviews(candidateID, candidateInt + 1);
                databaseManager.updateCandidatesStatisticsEntryByDate(DateUtils.getTodayDate(), candidateStatisticsCV, candidateStatisticsInt + 1, candidateStatisticsPlace);
              }

              if (statusChooser.getValue().toString().replace("'", "''").equals("Placement")) {
                databaseManager.updateJobsPlacementsEntry(jobID, jobPlace + 1);
                databaseManager.updateCandidatesAllPlacements(candidateID, candidatePlace + 1);
                databaseManager.updateCandidatesAllProcesses(candidateID, processes - 1);
                databaseManager.updateCandidatesStatisticsEntryByDate(DateUtils.getTodayDate(), candidateStatisticsCV, candidateStatisticsInt, candidateStatisticsPlace + 1);
              }

              if (statusChooser.getValue().toString().replace("'", "''").equals("Rejected")) {
                databaseManager.updateCandidatesAllRejections(candidateID, candidateRejec + 1);
                databaseManager.updateCandidatesAllProcesses(candidateID, processes - 1);

              }
            }


            if (currentStatus.equals("Interview")) {
              if (statusChooser.getValue().toString().replace("'", "''").equals("CV sent")) {
                databaseManager.updateJobsInterviewsEntry(jobID, jobInt - 1);
                databaseManager.updateCandidatesAllInterviews(candidateID, candidateInt - 1);
                databaseManager.updateCandidatesStatisticsEntryByDate(DateUtils.getTodayDate(), candidateStatisticsCV, candidateStatisticsInt - 1, candidateStatisticsPlace);
              }

              if (statusChooser.getValue().toString().replace("'", "''").equals("Placement")) {
                databaseManager.updateJobsPlacementsEntry(jobID, jobPlace + 1);
                databaseManager.updateCandidatesAllPlacements(candidateID, candidatePlace + 1);
                databaseManager.updateCandidatesAllProcesses(candidateID, processes - 1);
                databaseManager.updateCandidatesStatisticsEntryByDate(DateUtils.getTodayDate(), candidateStatisticsCV, candidateStatisticsInt, candidateStatisticsPlace + 1);
              }

              if (statusChooser.getValue().toString().replace("'", "''").equals("Rejected")) {
                databaseManager.updateCandidatesAllRejections(candidateID, candidateRejec + 1);
                databaseManager.updateCandidatesAllProcesses(candidateID, processes - 1);

              }
            }


            if (currentStatus.equals("Placement")) {

              if (statusChooser.getValue().toString().replace("'", "''").equals("CV sent")) {
                databaseManager.updateJobsPlacementsEntry(jobID, jobPlace - 1);
                databaseManager.updateCandidatesAllProcesses(candidateID, processes + 1);
                databaseManager.updateCandidatesStatisticsEntryByDate(DateUtils.getTodayDate(), candidateStatisticsCV, candidateStatisticsInt, candidateStatisticsPlace - 1);
              }

              if (statusChooser.getValue().toString().replace("'", "''").equals("Interview")) {
                databaseManager.updateJobsPlacementsEntry(jobID, jobPlace - 1);
                databaseManager.updateCandidatesAllInterviews(candidateID, candidateInt + 1);
                databaseManager.updateCandidatesAllPlacements(candidateID, candidatePlace - 1);
                databaseManager.updateCandidatesAllProcesses(candidateID, processes + 1);
                databaseManager.updateCandidatesStatisticsEntryByDate(DateUtils.getTodayDate(), candidateStatisticsCV, candidateStatisticsInt, candidateStatisticsPlace - 1);
              }

              if (statusChooser.getValue().toString().replace("'", "''").equals("Rejected")) {
                databaseManager.updateCandidatesAllRejections(candidateID, candidateRejec + 1);
                databaseManager.updateCandidatesAllPlacements(candidateID, candidatePlace - 1);
                databaseManager.updateCandidatesStatisticsEntryByDate(DateUtils.getTodayDate(), candidateStatisticsCV, candidateStatisticsInt, candidateStatisticsPlace - 1);
              }
            }

          } catch (SQLException e) {
            e.printStackTrace();
          }

        }

      }
    });

  }

}
