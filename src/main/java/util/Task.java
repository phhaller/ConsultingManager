package util;

import database.DatabaseManager;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.InnerShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import view.TasksWindowController;

import java.sql.SQLException;

public class Task extends AnchorPane {

  private int x;
  private int y;
  private int id;
  private String urgency;
  private String dueDate;
  private String task;
  private String type;
  private Rectangle editRec;
  private Rectangle deleteRec;
  private AnchorPane rootPane;
  private TasksWindowController tasksWindowController;
  private DatabaseManager databaseManager;

  public Task(int x, int y, int id, String urgency, String dueDate, String task, String type, AnchorPane rootPane,
              TasksWindowController tasksWindowController, DatabaseManager databaseManager) {
    this.x = x;
    this.y = y;
    this.id = id;
    this.urgency = urgency;
    this.dueDate = dueDate;
    this.task = task;
    this.type = type;
    this.rootPane = rootPane;
    this.tasksWindowController = tasksWindowController;
    this.databaseManager = databaseManager;

    createTask();
    createHandlers();
  }


  private void createTask() {

    setPrefSize(900, 100);
    setTranslateX(x);
    setTranslateY(y);
    getStyleClass().add("taskPane");

    Circle urgencyCircle = new Circle();
    urgencyCircle.setRadius(7);
    urgencyCircle.setCenterX(30);
    urgencyCircle.setCenterY(50);
    urgencyCircle.getStyleClass().add(urgency);
    urgencyCircle.setEffect(new InnerShadow(BlurType.GAUSSIAN, Color.rgb(0, 0, 0, 0.3), 10, 0.1, 0.0, 0.0));

    Line divider1 = new Line(50, 20, 50, 80);
    divider1.getStyleClass().add("dividerLine");

    Label dueDateLabel = new Label(dueDate);
    dueDateLabel.setPrefSize(100, 50);
    dueDateLabel.setTranslateX(55);
    dueDateLabel.setTranslateY(25);
    dueDateLabel.getStyleClass().add("dueDateLabel");

    Line divider2 = new Line(160, 20, 160, 80);
    divider2.getStyleClass().add("dividerLine");

    Label taskLabel = new Label(task);
    taskLabel.setPrefSize(550, 70);
    taskLabel.setTranslateX(180);
    taskLabel.setTranslateY(15);
    taskLabel.getStyleClass().add("taskLabel");

    Line divider3 = new Line(740, 20, 740, 80);
    divider3.getStyleClass().add("dividerLine");

    Label typeLabel = new Label(type);
    typeLabel.setPrefSize(90, 50);
    typeLabel.setTranslateX(750);
    typeLabel.setTranslateY(25);
    typeLabel.getStyleClass().add("typeLabel");

    Line divider4 = new Line(850, 20, 850, 80);
    divider4.getStyleClass().add("dividerLine");

    editRec = new Rectangle(20, 20);
    editRec.setTranslateX(865);
    editRec.setTranslateY(25);
    editRec.getStyleClass().add("editRec");
    editRec.setVisible(false);

    deleteRec = new Rectangle(20, 20);
    deleteRec.setTranslateX(865);
    deleteRec.setTranslateY(55);
    deleteRec.getStyleClass().add("deleteRec");
    deleteRec.setVisible(false);

    getChildren().addAll(urgencyCircle, divider1, dueDateLabel, divider2, taskLabel, divider3, typeLabel,
        divider4, editRec, deleteRec);

  }


  private void createHandlers() {

    addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        editRec.setVisible(true);
        deleteRec.setVisible(true);
      }
    });

    addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        editRec.setVisible(false);
        deleteRec.setVisible(false);
      }
    });

    editRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        AddOrEditTask addOrEditTask = new AddOrEditTask(rootPane, tasksWindowController, databaseManager);
        addOrEditTask.edit(id, urgency, dueDate, task, type);
      }
    });

    deleteRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        try {
          databaseManager.deleteTaskEntry(id);
          tasksWindowController.updateWindow(databaseManager.readAllTaskEntries());
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    });

  }


  public int getTaskId() {
    return id;
  }

  public String getUrgency() {
    return urgency;
  }

  public String getDueDate() {
    return dueDate;
  }

  public String getTask() {
    return task;
  }

  public String getType() {
    return type;
  }

}
