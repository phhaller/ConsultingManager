package util;

import database.DatabaseManager;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import view.ActivityWindowController;

import java.sql.SQLException;
import java.time.LocalDate;

public class AddOrEditActivity {

  private AnchorPane background;
  private ActivityWindowController activityWindowController;
  private DatabaseManager databaseManager;

  private AnchorPane addOrEditPane;
  private Rectangle backRec;
  private TextArea activityTextArea;
  private Rectangle increaseRec;
  private Rectangle decreaseRec;
  private Label currentLabel;
  private ComboBox unitChooser;
  private Label saveLabel;

  private boolean editing = false;
  private int activityID;
  private String unit;
  private String currentDate;


  public AddOrEditActivity(AnchorPane background, ActivityWindowController windowController, DatabaseManager databaseManager) {

    this.background = background;
    this.activityWindowController = windowController;
    this.databaseManager = databaseManager;
    createWindow();
    createHandlers();

  }


  private void createWindow() {

    addOrEditPane = new AnchorPane();
    addOrEditPane.setPrefSize(950, 800);
    addOrEditPane.getStyleClass().add("addOrEditBackground");

    backRec = new Rectangle(20, 20);
    backRec.setTranslateX(25);
    backRec.setTranslateY(25);
    backRec.getStyleClass().add("backRec");

    AnchorPane formPane = new AnchorPane();
    formPane.setPrefSize(600, 320);
    formPane.setTranslateX(175);
    formPane.setTranslateY(260);
    formPane.getStylesheets().clear();
    formPane.getStylesheets().add("/css/addOrEditStylesheet.css");
    formPane.getStyleClass().add("formPane");

    Label activityLabel = new Label("Activity");
    activityLabel.setTranslateX(50);
    activityLabel.setTranslateY(40);
    activityLabel.getStyleClass().add("formLabel");

    activityTextArea = new TextArea();
    activityTextArea.setPrefSize(500, 45);
    activityTextArea.setTranslateX(50);
    activityTextArea.setTranslateY(60);

    Label goalLabel = new Label("Goal");
    goalLabel.setTranslateX(300);
    goalLabel.setTranslateY(135);
    goalLabel.getStyleClass().add("formLabel");

    decreaseRec = new Rectangle(16, 16);
    decreaseRec.setTranslateX(300);
    decreaseRec.setTranslateY(172);
    decreaseRec.getStyleClass().add("decreaseRec");

    currentLabel = new Label("0");
    currentLabel.setPrefSize(50, 50);
    currentLabel.setTranslateX(330);
    currentLabel.setTranslateY(155);
    currentLabel.getStyleClass().add("currentLabel");

    increaseRec = new Rectangle(16, 16);
    increaseRec.setTranslateX(394);
    increaseRec.setTranslateY(172);
    increaseRec.getStyleClass().add("increaseRec");

    Label unitLabel = new Label("Unit");
    unitLabel.setTranslateX(50);
    unitLabel.setTranslateY(135);
    unitLabel.getStyleClass().add("formLabel");

    unitChooser = new ComboBox();
    unitChooser.setPrefSize(200, 40);
    unitChooser.setTranslateX(50);
    unitChooser.setTranslateY(155);
    unitChooser.getItems().addAll("day", "week", "month", "year");
    unitChooser.getSelectionModel().select("day");

    saveLabel = new Label("Save");
    saveLabel.setPrefSize(200, 40);
    saveLabel.setTranslateX(50);
    saveLabel.setTranslateY(230);
    saveLabel.getStyleClass().add("saveLabel");

    formPane.getChildren().addAll(activityLabel, activityTextArea, goalLabel, decreaseRec, currentLabel, increaseRec,
        unitLabel, unitChooser, saveLabel);

    addOrEditPane.getChildren().addAll(backRec, formPane);

    background.getChildren().add(addOrEditPane);

  }


  public void edit(int activityID, String activity, int goal, String unit, String currentDate) {

    editing = true;
    this.activityID = activityID;
    this.unit = unit;
    this.currentDate = currentDate;
    activityTextArea.setText(activity);
    currentLabel.setText(String.valueOf(goal));
    unitChooser.getSelectionModel().select(unit);

  }


  private void createHandlers() {

    backRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        background.getChildren().remove(addOrEditPane);

      }
    });

    activityTextArea.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
      @Override
      public void handle(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
          event.consume();
        }
      }
    });

    increaseRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        int current = Integer.parseInt(currentLabel.getText());
        currentLabel.setText(String.valueOf(current + 1));
      }
    });

    decreaseRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        int current = Integer.parseInt(currentLabel.getText());
        if (current > 0) {
          currentLabel.setText(String.valueOf(current - 1));
        }
      }
    });

    saveLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        if (!editing) {

          try {
            databaseManager.createActivitiesEntry(
                activityTextArea.getText().replace("'", "''"),
                Integer.parseInt(currentLabel.getText()),
                unitChooser.getValue().toString(),
                0,
                getCurrentDate(unitChooser.getValue().toString()));
            background.getChildren().remove(addOrEditPane);
            activityWindowController.updateWindow(databaseManager.readAllActivitiesEntries());
          } catch (SQLException e) {
            e.printStackTrace();
          }

        } else {

          String date = unit.equals(unitChooser.getValue().toString()) ? currentDate : getCurrentDate(unitChooser.getValue().toString());

          try {
            databaseManager.updateActivitiesEntryAfterEdit(
                activityID,
                activityTextArea.getText().replace("'", "''"),
                Integer.parseInt(currentLabel.getText()),
                unitChooser.getValue().toString(),
                date);
            background.getChildren().remove(addOrEditPane);
            activityWindowController.updateWindow(databaseManager.readAllActivitiesEntries());
          } catch (SQLException e) {
            e.printStackTrace();
          }

        }
      }
    });

  }


  private String getCurrentDate(String unit) {

    String currentDate = "";
    LocalDate now = LocalDate.now();

    switch (unit) {
      case "day":
        currentDate = "d_" + DateUtils.formatDate(now);
        break;
      case "week":
        currentDate = "w_" + DateUtils.getWeekFromDate(now);
        break;
      case "month":
        currentDate = "m_" + DateUtils.getMonthNumberFromDate(now);
        break;
      case "year":
        currentDate = "y_" + DateUtils.getYearFromDate(now);
        break;
    }

    return currentDate;

  }

}
