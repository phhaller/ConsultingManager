package util;

import database.DatabaseManager;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import view.ActivityWindowController;

import java.sql.SQLException;

public class Activity extends AnchorPane {

  private int x;
  private int y;
  private int id;
  private String activity;
  private int goal;
  private String unit;
  private int current;
  private String currentDate;
  private Rectangle increaseRec;
  private Rectangle decreaseRec;
  private Label currentLabel;
  private Rectangle checkRec;
  private Rectangle editRec;
  private Rectangle deleteRec;
  private AnchorPane rootPane;
  private ActivityWindowController activityWindowController;
  private DatabaseManager databaseManager;

  public Activity(int x, int y, int id, String activity, int goal, String unit, int current, String currentDate,
                  AnchorPane rootPane, ActivityWindowController activityWindowController, DatabaseManager databaseManager) {
    this.x = x;
    this.y = y;
    this.id = id;
    this.activity = activity;
    this.goal = goal;
    this.unit = unit;
    this.current = current;
    this.currentDate = currentDate;
    this.rootPane = rootPane;
    this.activityWindowController = activityWindowController;
    this.databaseManager = databaseManager;

    createActivity();
    createHandlers();
  }


  private void createActivity() {

    setPrefSize(900, 100);
    setTranslateX(x);
    setTranslateY(y);
    getStyleClass().add("activityPane");

    Label activityLabel = new Label(activity);
    activityLabel.setPrefSize(450, 70);
    activityLabel.setTranslateX(30);
    activityLabel.setTranslateY(15);
    activityLabel.getStyleClass().add("activityLabel");

    Line divider1 = new Line(490, 20, 490, 80);
    divider1.getStyleClass().add("dividerLine");

    Label goalLabel = new Label(goal + "   /   " + unit);
    goalLabel.setPrefSize(150, 50);
    goalLabel.setTranslateX(500);
    goalLabel.setTranslateY(25);
    goalLabel.getStyleClass().add("goalLabel");

    Line divider2 = new Line(660, 20, 660, 80);
    divider2.getStyleClass().add("dividerLine");

    decreaseRec = new Rectangle(16, 16);
    decreaseRec.setTranslateX(700);
    decreaseRec.setTranslateY(42);
    decreaseRec.getStyleClass().add("decreaseRec");

    currentLabel = new Label(String.valueOf(current));
    currentLabel.setPrefSize(50, 50);
    currentLabel.setTranslateX(730);
    currentLabel.setTranslateY(25);
    currentLabel.getStyleClass().add("currentLabel");

    increaseRec = new Rectangle(16, 16);
    increaseRec.setTranslateX(794);
    increaseRec.setTranslateY(42);
    increaseRec.getStyleClass().add("increaseRec");

    checkRec = new Rectangle(26, 26);
    checkRec.setTranslateX(454);
    checkRec.setTranslateY(37);
    checkRec.getStyleClass().add("checkRec");
    if (current < goal) {
      checkRec.setVisible(false);
    }

    Line divider3 = new Line(850, 20, 850, 80);
    divider3.getStyleClass().add("dividerLine");

    editRec = new Rectangle(20, 20);
    editRec.setTranslateX(865);
    editRec.setTranslateY(25);
    editRec.getStyleClass().add("editRec");
    editRec.setVisible(false);

    deleteRec = new Rectangle(20, 20);
    deleteRec.setTranslateX(865);
    deleteRec.setTranslateY(55);
    deleteRec.getStyleClass().add("deleteRec");
    deleteRec.setVisible(false);



    getChildren().addAll(activityLabel, divider1, goalLabel, divider2, decreaseRec, currentLabel, increaseRec, checkRec,
        divider3, editRec, deleteRec);

  }


  private void createHandlers() {

    increaseRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        int current = Integer.parseInt(currentLabel.getText());
        currentLabel.setText(String.valueOf(current + 1));
        try {
          databaseManager.updateActivitiesEntryCurrent(id, current + 1);
          int todayCurrent = Integer.parseInt(databaseManager.readActivitiesStatisticsEntryByDateAndId(DateUtils.getTodayDate(), id).split("§")[3]);
          databaseManager.updateActivitiesStatisticsEntryByRefId(DateUtils.getTodayDate(), id, todayCurrent + 1);
        } catch (SQLException e) {
          e.printStackTrace();
        }
        if (current + 1 >= goal && !checkRec.isVisible()) {
          checkRec.setVisible(true);
        }
      }
    });

    decreaseRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        int current = Integer.parseInt(currentLabel.getText());
        if (current > 0) {
          currentLabel.setText(String.valueOf(current - 1));
          try {
            databaseManager.updateActivitiesEntryCurrent(id, current - 1);
            int todayCurrent = Integer.parseInt(databaseManager.readActivitiesStatisticsEntryByDateAndId(DateUtils.getTodayDate(), id).split("§")[3]);
            databaseManager.updateActivitiesStatisticsEntryByRefId(DateUtils.getTodayDate(), id, todayCurrent - 1);
          } catch (SQLException e) {
            e.printStackTrace();
          }
          if (current - 1 < goal && checkRec.isVisible()) {
            checkRec.setVisible(false);
          }
        }
      }
    });

    addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        editRec.setVisible(true);
        deleteRec.setVisible(true);
      }
    });

    addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        editRec.setVisible(false);
        deleteRec.setVisible(false);
      }
    });

    editRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        AddOrEditActivity addOrEditActivity = new AddOrEditActivity(rootPane, activityWindowController, databaseManager);
        addOrEditActivity.edit(id, activity, goal, unit, currentDate);
      }
    });

    deleteRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        try {
          databaseManager.deleteActivitiesEntry(id);
          activityWindowController.updateWindow(databaseManager.readAllActivitiesEntries());
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    });

  }


  public int getTaskId() {
    return id;
  }

  public int getGoal() {
    return goal;
  }

  public String getActivity() {
    return activity;
  }

}
