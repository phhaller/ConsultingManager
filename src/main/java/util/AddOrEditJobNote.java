package util;

import database.DatabaseManager;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import view.JobsNotesWindowController;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class AddOrEditJobNote {

  private AnchorPane background;
  private JobsNotesWindowController jobsNotesWindowController;
  private DatabaseManager databaseManager;

  private AnchorPane addOrEditPane;
  private Rectangle backRec;
  private ComboBox companyChooser;
  private ComboBox jobsChooser;
  private TextArea noteTextArea;
  private Label saveLabel;

  private boolean editing = false;
  private int taskID;


  public AddOrEditJobNote(AnchorPane background, JobsNotesWindowController windowController, DatabaseManager databaseManager) {

    this.background = background;
    this.jobsNotesWindowController = windowController;
    this.databaseManager = databaseManager;
    createWindow();
    createHandlers();

  }


  private void createWindow() {

    addOrEditPane = new AnchorPane();
    addOrEditPane.setPrefSize(950, 800);
    addOrEditPane.getStyleClass().add("addOrEditBackground");

    backRec = new Rectangle(20, 20);
    backRec.setTranslateX(25);
    backRec.setTranslateY(25);
    backRec.getStyleClass().add("backRec");

    AnchorPane formPane = new AnchorPane();
    formPane.setPrefSize(600, 555);
    formPane.setTranslateX(175);
    formPane.setTranslateY(157);
    formPane.getStylesheets().clear();
    formPane.getStylesheets().add("/css/addOrEditStylesheet.css");
    formPane.getStyleClass().add("formPane");

    Label companyLabel = new Label("Company");
    companyLabel.setTranslateX(50);
    companyLabel.setTranslateY(40);
    companyLabel.getStyleClass().add("formLabel");

    companyChooser = new ComboBox();
    companyChooser.setPrefSize(500, 40);
    companyChooser.setTranslateX(50);
    companyChooser.setTranslateY(60);

    try {
      ArrayList<String> companies = databaseManager.readAllCompaniesEntries();

      Collections.sort(companies, new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {

          String[] a = o1.split("§");
          String[] b = o2.split("§");

          return a[1].compareTo(b[1]);

        }
      });

      companyChooser.getItems().add("");

      for (String c : companies) {
        String[] s = c.split("§");
        companyChooser.getItems().add(s[1]);
      }

      companyChooser.getSelectionModel().select("");

    } catch (SQLException e) {
      e.printStackTrace();
    }

    Label jobTitleLabel = new Label("Job Title");
    jobTitleLabel.setTranslateX(50);
    jobTitleLabel.setTranslateY(130);
    jobTitleLabel.getStyleClass().add("formLabel");

    jobsChooser = new ComboBox();
    jobsChooser.setPrefSize(500, 40);
    jobsChooser.setTranslateX(50);
    jobsChooser.setTranslateY(150);

    try {
      addJobsToChooser(databaseManager.readAllJobsEntries());
    } catch (SQLException e) {
      e.printStackTrace();
    }

    Label noteLabel = new Label("Note");
    noteLabel.setTranslateX(50);
    noteLabel.setTranslateY(220);
    noteLabel.getStyleClass().add("formLabel");

    noteTextArea = new TextArea();
    noteTextArea.setPrefSize(500, 200);
    noteTextArea.setTranslateX(50);
    noteTextArea.setTranslateY(240);

    saveLabel = new Label("Save");
    saveLabel.setPrefSize(240, 40);
    saveLabel.setTranslateX(50);
    saveLabel.setTranslateY(470);
    saveLabel.getStyleClass().add("saveLabel");

    formPane.getChildren().addAll(companyLabel, companyChooser, jobTitleLabel, jobsChooser, noteLabel, noteTextArea, saveLabel);

    addOrEditPane.getChildren().addAll(backRec, formPane);

    background.getChildren().add(addOrEditPane);

  }


  private void addJobsToChooser(ArrayList<String> list) {

    jobsChooser.getItems().clear();

    Collections.sort(list, new Comparator<String>() {
      @Override
      public int compare(String o1, String o2) {

        String[] a = o1.split("§");
        String[] b = o2.split("§");

        return a[1].compareTo(b[1]);

      }
    });

    jobsChooser.getItems().add("");

    for (String c : list) {
      String[] s = c.split("§");
      jobsChooser.getItems().add(s[1]);
    }

    jobsChooser.getSelectionModel().select("");

  }


  public void edit(int taskID, String company, String job, String note) {

    editing = true;
    this.taskID = taskID;
    companyChooser.getSelectionModel().select(company);
    jobsChooser.getSelectionModel().select(job);
    noteTextArea.setText(note);

  }


  private void createHandlers() {

    backRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        background.getChildren().remove(addOrEditPane);
      }
    });

    companyChooser.valueProperty().addListener(new ChangeListener() {
      @Override
      public void changed(ObservableValue observable, Object oldValue, Object newValue) {

        if (newValue.equals("")) {
          try {
            addJobsToChooser(databaseManager.readAllJobsEntries());
          } catch (SQLException e) {
            e.printStackTrace();
          }
        } else {
          try {
            addJobsToChooser(databaseManager.readJobsEntriesByCompany(newValue.toString()));
          } catch (SQLException e) {
            e.printStackTrace();
          }
        }

      }
    });

    saveLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        if (!editing) {

          try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            databaseManager.createJobsNotesEntry(
                companyChooser.getValue().toString().replace("'", "''"),
                jobsChooser.getValue().toString().replace("'", "''"),
                noteTextArea.getText().replace("'", "''"),
                dtf.format(LocalDate.now()));
            background.getChildren().remove(addOrEditPane);
            jobsNotesWindowController.updateWindow(databaseManager.readAllJobsNotesEntries());
          } catch (SQLException e) {
            e.printStackTrace();
          }

        } else {

          try {
            databaseManager.updateJobsNotesEntryAfterEdit(
                taskID,
                companyChooser.getValue().toString().replace("'", "''"),
                jobsChooser.getValue().toString().replace("'", "''"),
                noteTextArea.getText().replace("'", "''"));
            background.getChildren().remove(addOrEditPane);
            jobsNotesWindowController.updateWindow(databaseManager.readAllJobsNotesEntries());
          } catch (SQLException e) {
            e.printStackTrace();
          }

        }
      }
    });

  }

}
