package util;

import database.DatabaseManager;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import view.CandidateAllWindowController;

import java.sql.SQLException;
import java.util.ArrayList;

public class Candidate extends AnchorPane {

  private int x;
  private int y;
  private int id;
  private String firstName;
  private String lastName;
  private String job;
  private String salary;
  private String processes;
  private Rectangle goToRec;
  private Rectangle editRec;
  private Rectangle deleteRec;
  private AnchorPane rootPane;
  private CandidateAllWindowController candidateAllWindowController;
  private DatabaseManager databaseManager;

  public Candidate(int x, int y, int id, String firstName, String lastName, String job, String salary, String processes,
                   AnchorPane rootPane, CandidateAllWindowController candidateAllWindowController, DatabaseManager databaseManager) {
    this.x = x;
    this.y = y;
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.job = job;
    this.salary = salary;
    this.processes = processes;
    this.rootPane = rootPane;
    this.candidateAllWindowController = candidateAllWindowController;
    this.databaseManager = databaseManager;

    createJob();
    createHandlers();
  }


  private void createJob() {

    setPrefSize(900, 100);
    setTranslateX(x);
    setTranslateY(y);
    getStyleClass().add("candidatePane");

    Label nameLabel = new Label(firstName + " " + lastName);
    nameLabel.setPrefSize(350, 50);
    nameLabel.setTranslateX(30);
    nameLabel.setTranslateY(25);
    nameLabel.getStyleClass().add("candidateNameLabel");

    Line divider1 = new Line(390, 20, 390, 80);
    divider1.getStyleClass().add("dividerLine");

    Label jobLabel = new Label(job);
    jobLabel.setPrefSize(300, 50);
    jobLabel.setTranslateX(400);
    jobLabel.setTranslateY(25);
    jobLabel.getStyleClass().add("candidateJobLabel");

    Line divider2 = new Line(710, 20, 710, 80);
    divider2.getStyleClass().add("dividerLine");

    Label processesLabel = new Label(processes);
    processesLabel.setPrefSize(70, 50);
    processesLabel.setTranslateX(720);
    processesLabel.setTranslateY(25);
    processesLabel.getStyleClass().add("processesLabel");

    Line divider3 = new Line(800, 20, 800, 80);
    divider3.getStyleClass().add("dividerLine");

    goToRec = new Rectangle(15, 15);
    goToRec.setTranslateX(818);
    goToRec.setTranslateY(40);
    goToRec.getStyleClass().add("goToRec");
    goToRec.setVisible(false);

    Line divider4 = new Line(850, 20, 850, 80);
    divider4.getStyleClass().add("dividerLine");

    editRec = new Rectangle(20, 20);
    editRec.setTranslateX(865);
    editRec.setTranslateY(25);
    editRec.getStyleClass().add("editRec");
    editRec.setVisible(false);

    deleteRec = new Rectangle(20, 20);
    deleteRec.setTranslateX(865);
    deleteRec.setTranslateY(55);
    deleteRec.getStyleClass().add("deleteRec");
    deleteRec.setVisible(false);

    getChildren().addAll(nameLabel, divider1, jobLabel, divider2, processesLabel, divider3, goToRec, divider4,
        editRec, deleteRec);

  }


  private void createHandlers() {

    addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        goToRec.setVisible(true);
        editRec.setVisible(true);
        deleteRec.setVisible(true);
      }
    });

    addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        goToRec.setVisible(false);
        editRec.setVisible(false);
        deleteRec.setVisible(false);
      }
    });

    goToRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        try {
          String candidateDetails = databaseManager.readCandidatesAllByID(id);
          ArrayList<String> jobsDetails = databaseManager.readCandidatesInProcessEntriesByCandidate(firstName + " " + lastName);
          new CandidateEntry(rootPane, candidateDetails, jobsDetails);
        } catch (SQLException e) {
          e.printStackTrace();
        }

      }
    });

    editRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        AddOrEditCandidatesAll addOrEditCandidatesAll = new AddOrEditCandidatesAll(rootPane, candidateAllWindowController, databaseManager);
        addOrEditCandidatesAll.edit(id, firstName, lastName, job, salary);

      }
    });

    deleteRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        try {
          databaseManager.deleteCandidatesAllEntry(id);
          candidateAllWindowController.updateWindow(databaseManager.readAllCandidatesAllEntries());
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    });

  }


  public int getTaskId() {
    return id;
  }

}
