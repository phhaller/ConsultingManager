package util;

import database.DatabaseManager;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import view.CompaniesWindowController;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class AddOrEditCompany {

  private AnchorPane background;
  private CompaniesWindowController companiesWindowController;
  private DatabaseManager databaseManager;

  private AnchorPane addOrEditPane;
  private Rectangle backRec;
  private TextField companyNameTextField;
  private Label saveLabel;
  private Label errorLabel;

  private boolean editing = false;
  private int taskID;
  private String companyName;


  public AddOrEditCompany(AnchorPane background, CompaniesWindowController windowController, DatabaseManager databaseManager) {

    this.background = background;
    this.companiesWindowController = windowController;
    this.databaseManager = databaseManager;
    createWindow();
    createHandlers();

  }


  private void createWindow() {

    addOrEditPane = new AnchorPane();
    addOrEditPane.setPrefSize(950, 800);
    addOrEditPane.getStyleClass().add("addOrEditBackground");

    backRec = new Rectangle(20, 20);
    backRec.setTranslateX(25);
    backRec.setTranslateY(25);
    backRec.getStyleClass().add("backRec");

    AnchorPane formPane = new AnchorPane();
    formPane.setPrefSize(600, 220);
    formPane.setTranslateX(175);
    formPane.setTranslateY(177);
    formPane.getStylesheets().clear();
    formPane.getStylesheets().add("/css/addOrEditStylesheet.css");
    formPane.getStyleClass().add("formPane");

    Label companyNameLabel = new Label("Company Name");
    companyNameLabel.setTranslateX(50);
    companyNameLabel.setTranslateY(40);
    companyNameLabel.getStyleClass().add("formLabel");

    companyNameTextField = new TextField();
    companyNameTextField.setPrefSize(500, 40);
    companyNameTextField.setTranslateX(50);
    companyNameTextField.setTranslateY(60);

    saveLabel = new Label("Save");
    saveLabel.setPrefSize(240, 40);
    saveLabel.setTranslateX(50);
    saveLabel.setTranslateY(130);
    saveLabel.getStyleClass().add("saveLabel");

    errorLabel = new Label();
    errorLabel.setTranslateX(50);
    errorLabel.setTranslateY(105);
    errorLabel.getStyleClass().add("errorLabel");
    errorLabel.setVisible(false);

    formPane.getChildren().addAll(companyNameLabel, companyNameTextField, saveLabel, errorLabel);

    addOrEditPane.getChildren().addAll(backRec, formPane);

    background.getChildren().add(addOrEditPane);

  }


  public void edit(int taskID, String companyName) {

    editing = true;
    this.taskID = taskID;
    this.companyName = companyName;
    companyNameTextField.setText(companyName);

  }


  private void createHandlers() {

    backRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        background.getChildren().remove(addOrEditPane);
      }
    });


    saveLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        if (!editing) {

          try {

            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            if (databaseManager.containsCompany(companyNameTextField.getText().replace("'", "''"))) {

              errorLabel.setText("Company already exists!");
              errorLabel.setVisible(true);

            } else {

              errorLabel.setVisible(false);
              databaseManager.createCompaniesEntry(
                  companyNameTextField.getText().replace("'", "''"),
                  dtf.format(LocalDate.now()), dtf.format(LocalDate.now()));
              background.getChildren().remove(addOrEditPane);
              companiesWindowController.updateWindow(databaseManager.readAllCompaniesEntries());

            }

          } catch (SQLException e) {
            e.printStackTrace();
          }

          System.out.println("SAVE TASK");
        } else {

          try {
            databaseManager.updateCompanyNameEntry(taskID, companyNameTextField.getText().replace("'", "''"));
            databaseManager.updateJobsCompanyEntry(companyName, companyNameTextField.getText().replace("'", "''"));
            background.getChildren().remove(addOrEditPane);
            companiesWindowController.updateWindow(databaseManager.readAllCompaniesEntries());
          } catch (SQLException e) {
            e.printStackTrace();
          }

        }
      }
    });

  }

}
