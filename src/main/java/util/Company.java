package util;

import database.DatabaseManager;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import view.CompaniesWindowController;

import java.sql.SQLException;
import java.util.ArrayList;

public class Company extends AnchorPane {

  private int x;
  private int y;
  private int id;
  private String companyName;
  private Rectangle goToRec;
  private Rectangle editRec;
  private Rectangle deleteRec;
  private AnchorPane rootPane;
  private CompaniesWindowController companiesWindowController;
  private DatabaseManager databaseManager;

  public Company(int x, int y, int id, String companyName, AnchorPane rootPane, CompaniesWindowController companiesWindowController,
                 DatabaseManager databaseManager) {
    this.x = x;
    this.y = y;
    this.id = id;
    this.companyName = companyName;
    this.rootPane = rootPane;
    this.companiesWindowController = companiesWindowController;
    this.databaseManager = databaseManager;

    createCompany();
    createHandlers();
  }


  private void createCompany() {

    setPrefSize(900, 100);
    setTranslateX(x);
    setTranslateY(y);
    getStyleClass().add("companyPane");

    Label companyNameLabel = new Label(companyName);
    companyNameLabel.setPrefSize(700, 50);
    companyNameLabel.setTranslateX(30);
    companyNameLabel.setTranslateY(25);
    companyNameLabel.getStyleClass().add("companyNameLabel");

    Line divider1 = new Line(800, 20, 800, 80);
    divider1.getStyleClass().add("dividerLine");

    goToRec = new Rectangle(15, 15);
    goToRec.setTranslateX(818);
    goToRec.setTranslateY(40);
    goToRec.getStyleClass().add("goToRec");
    goToRec.setVisible(false);

    Line divider2 = new Line(850, 20, 850, 80);
    divider2.getStyleClass().add("dividerLine");

    editRec = new Rectangle(20, 20);
    editRec.setTranslateX(865);
    editRec.setTranslateY(25);
    editRec.getStyleClass().add("editRec");
    editRec.setVisible(false);

    deleteRec = new Rectangle(20, 20);
    deleteRec.setTranslateX(865);
    deleteRec.setTranslateY(55);
    deleteRec.getStyleClass().add("deleteRec");
    deleteRec.setVisible(false);

    getChildren().addAll(companyNameLabel, divider1, goToRec, divider2, editRec, deleteRec);

  }


  private void createHandlers() {

    addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        goToRec.setVisible(true);
        editRec.setVisible(true);
        deleteRec.setVisible(true);
      }
    });

    addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        goToRec.setVisible(false);
        editRec.setVisible(false);
        deleteRec.setVisible(false);
      }
    });

    goToRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        try {
          String companiesDetails = databaseManager.readCompanyByID(id);
          ArrayList<String> jobsDetails = databaseManager.readJobsEntriesByCompany(companyName);
          new CompanyEntry(rootPane, companiesDetails, jobsDetails);
        } catch (SQLException e) {
          e.printStackTrace();
        }

      }
    });

    editRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        AddOrEditCompany addOrEditCompany = new AddOrEditCompany(rootPane, companiesWindowController, databaseManager);
        addOrEditCompany.edit(id, companyName);

      }
    });

    deleteRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        try {
          databaseManager.deleteCompaniesEntry(id);
          companiesWindowController.updateWindow(databaseManager.readAllCompaniesEntries());
        } catch (SQLException e) {
          e.printStackTrace();
        }

      }
    });

  }


  public int getTaskId() {
    return id;
  }

  public String getCompanyName() {
    return companyName;
  }

}
